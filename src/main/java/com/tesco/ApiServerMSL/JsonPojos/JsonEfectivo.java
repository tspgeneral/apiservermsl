package com.tesco.ApiServerMSL.JsonPojos;

public class JsonEfectivo {
	private int idEfectivo;
	private String moneda;
	
	public JsonEfectivo(int idEfectivo, String moneda) {
		super();
		this.idEfectivo = idEfectivo;
		this.moneda = moneda;
	}

	public int getIdEfectivo() {
		return idEfectivo;
	}

	public void setIdEfectivo(int idEfectivo) {
		this.idEfectivo = idEfectivo;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Override
	public String toString() {
		return "JsonEfectivo [idEfectivo=" + idEfectivo + ", moneda=" + moneda + "]";
	}
	
	
}
