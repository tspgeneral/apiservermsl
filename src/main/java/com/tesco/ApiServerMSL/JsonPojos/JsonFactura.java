package com.tesco.ApiServerMSL.JsonPojos;

public class JsonFactura {
	
	private int idFactura;
	private float montoTotal;
	private String fecha;
	private String tipo;
	
	private int idEfectivo;
	
	private long cbu;
	private String banco;
	private String titular;
	
	private long numTargeta;
	private String fechaVencimiento;
	private int codSeguridad;
	private int cuotas;
	
	public JsonFactura(){
		super();
	}
	
	public JsonFactura(int idFactura, float montoTotal, String fecha, String tipo, int idEfectivo, long cbu,
			String banco, String titular, long numTargeta, String fechaVencimiento, int codSeguridad, int cuotas) {
		super();
		this.idFactura = idFactura;
		this.montoTotal = montoTotal;
		this.fecha = fecha;
		this.tipo = tipo;
		this.idEfectivo = idEfectivo;
		this.cbu = cbu;
		this.banco = banco;
		this.titular = titular;
		this.numTargeta = numTargeta;
		this.fechaVencimiento = fechaVencimiento;
		this.codSeguridad = codSeguridad;
		this.cuotas = cuotas;
	}

	public int getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}

	public float getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(float montoTotal) {
		this.montoTotal = montoTotal;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getIdEfectivo() {
		return idEfectivo;
	}

	public void setIdEfectivo(int idEfectivo) {
		this.idEfectivo = idEfectivo;
	}

	public long getCbu() {
		return cbu;
	}

	public void setCbu(long cbu) {
		this.cbu = cbu;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public long getNumTargeta() {
		return numTargeta;
	}

	public void setNumTargeta(long numTargeta) {
		this.numTargeta = numTargeta;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public int getCodSeguridad() {
		return codSeguridad;
	}

	public void setCodSeguridad(int codSeguridad) {
		this.codSeguridad = codSeguridad;
	}

	public int getCuotas() {
		return cuotas;
	}

	public void setCuotas(int cuotas) {
		this.cuotas = cuotas;
	}

	@Override
	public String toString() {
		return "JsonFactura [idFactura=" + idFactura + ", montoTotal=" + montoTotal + ", fecha=" + fecha + ", tipo="
				+ tipo + ", idEfectivo=" + idEfectivo + ", cbu=" + cbu + ", banco=" + banco + ", titular=" + titular
				+ ", numTargeta=" + numTargeta + ", fechaVencimiento=" + fechaVencimiento + ", codSeguridad="
				+ codSeguridad + ", cuotas=" + cuotas + "]";
	}
	
	
}
