package com.tesco.ApiServerMSL.JsonPojos;

public class JsonFacturaDato {
	
	private int idFactura;
	private float montoTotal;
	private String fecha;
	private String tipo;
	
	public JsonFacturaDato(int idFactura, float montoTotal, String fecha, String tipo) {
		super();
		this.idFactura = idFactura;
		this.montoTotal = montoTotal;
		this.fecha = fecha;
		this.tipo = tipo;
	}

	public int getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}

	public float getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(float montoTotal) {
		this.montoTotal = montoTotal;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "JsonFacturaDato [idFactura=" + idFactura + ", montoTotal=" + montoTotal + ", fecha=" + fecha + ", tipo="
				+ tipo + "]";
	}
	
	
}
