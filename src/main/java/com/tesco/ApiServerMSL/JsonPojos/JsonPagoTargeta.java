package com.tesco.ApiServerMSL.JsonPojos;

public class JsonPagoTargeta {
	
	private int idFactura;
	private float montoTotal;
	private String fecha;
	private String tipo;
	
	private long numTargeta;
	private String fechaVencimiento;
	private int codSeguridad;
	private int cuotas;
	
	
	public JsonPagoTargeta(){
		super();
	}
	
	public JsonPagoTargeta(int idFactura, float montoTotal, String fecha, String tipo, long numTargeta,
			String fechaVencimiento, int codSeguridad, int cuotas) {
		super();
		this.idFactura = idFactura;
		this.montoTotal = montoTotal;
		this.fecha = fecha;
		this.tipo = tipo;
		this.numTargeta = numTargeta;
		this.fechaVencimiento = fechaVencimiento;
		this.codSeguridad = codSeguridad;
		this.cuotas = cuotas;
	}
	public JsonPagoTargeta(float montoTotal, String fecha, String tipo, long numTargeta, String fechaVencimiento,
			int codSeguridad, int cuotas) {
		super();
		this.montoTotal = montoTotal;
		this.fecha = fecha;
		this.tipo = tipo;
		this.numTargeta = numTargeta;
		this.fechaVencimiento = fechaVencimiento;
		this.codSeguridad = codSeguridad;
		this.cuotas = cuotas;
	}
	public int getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}
	public float getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(float montoTotal) {
		this.montoTotal = montoTotal;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public long getNumTargeta() {
		return numTargeta;
	}
	public void setNumTargeta(long numTargeta) {
		this.numTargeta = numTargeta;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public int getCodSeguridad() {
		return codSeguridad;
	}
	public void setCodSeguridad(int codSeguridad) {
		this.codSeguridad = codSeguridad;
	}
	public int getCuotas() {
		return cuotas;
	}
	public void setCuotas(int cuotas) {
		this.cuotas = cuotas;
	}

	@Override
	public String toString() {
		return "JsonPagoTargeta [idFactura=" + idFactura + ", montoTotal=" + montoTotal + ", fecha=" + fecha + ", tipo="
				+ tipo + ", numTargeta=" + numTargeta + ", fechaVencimiento=" + fechaVencimiento + ", codSeguridad="
				+ codSeguridad + ", cuotas=" + cuotas + "]";
	}
	
	
}
