package com.tesco.ApiServerMSL.JsonPojos;

public class JsonReservaPago {
	private int idReserva;
	private String fechaIn;
	private String fechaOut;
	private String horaIn;
	private String horaOut;
	private int idServicio;
	private String servicioNombre;
	private float servicioCosto;
	
	public JsonReservaPago(int idReserva, String fechaIn, String fechaOut, String horaIn, String horaOut,
			int idServicio, String servicioNombre, float servicioCosto) {
		super();
		this.idReserva = idReserva;
		this.fechaIn = fechaIn;
		this.fechaOut = fechaOut;
		this.horaIn = horaIn;
		this.horaOut = horaOut;
		this.idServicio = idServicio;
		this.servicioNombre = servicioNombre;
		this.servicioCosto = servicioCosto;
	}

	public int getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}

	public String getFechaIn() {
		return fechaIn;
	}

	public void setFechaIn(String fechaIn) {
		this.fechaIn = fechaIn;
	}

	public String getFechaOut() {
		return fechaOut;
	}

	public void setFechaOut(String fechaOut) {
		this.fechaOut = fechaOut;
	}

	public String getHoraIn() {
		return horaIn;
	}

	public void setHoraIn(String horaIn) {
		this.horaIn = horaIn;
	}

	public String getHoraOut() {
		return horaOut;
	}

	public void setHoraOut(String horaOut) {
		this.horaOut = horaOut;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public String getServicioNombre() {
		return servicioNombre;
	}

	public void setServicioNombre(String servicioNombre) {
		this.servicioNombre = servicioNombre;
	}

	public float getServicioCosto() {
		return servicioCosto;
	}

	public void setServicioCosto(float servicioCosto) {
		this.servicioCosto = servicioCosto;
	}

	@Override
	public String toString() {
		return "JsonReservaPago [idReserva=" + idReserva + ", fechaIn=" + fechaIn + ", fechaOut=" + fechaOut
				+ ", horaIn=" + horaIn + ", horaOut=" + horaOut + ", idServicio=" + idServicio + ", servicioNombre="
				+ servicioNombre + ", servicioCosto=" + servicioCosto + "]";
	}
	
	
	
	

}
