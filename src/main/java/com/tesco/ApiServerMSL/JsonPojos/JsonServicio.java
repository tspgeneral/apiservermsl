package com.tesco.ApiServerMSL.JsonPojos;


public class JsonServicio{
	
	    private int idservicio;
	    private String nombre;
	    private String descripcion;
	    private float costo;
	    private int capacidadPersona;
	    private String tipo;
	    
	    public JsonServicio(){
	    	super();
	    }
	    
		public JsonServicio(int idservicio, String nombre, String descripcion, float costo, int capacidadPersona,
				String tipo) {
			super();
			this.idservicio = idservicio;
			this.nombre = nombre;
			this.descripcion = descripcion;
			this.costo = costo;
			this.capacidadPersona = capacidadPersona;
			this.tipo = tipo;
		}
		public int getIdservicio() {
			return idservicio;
		}
		public void setIdservicio(int idservicio) {
			this.idservicio = idservicio;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		public float getCosto() {
			return costo;
		}
		public void setCosto(float costo) {
			this.costo = costo;
		}
		public int getCapacidadPersona() {
			return capacidadPersona;
		}
		public void setCapacidadPersona(int capacidadPersona) {
			this.capacidadPersona = capacidadPersona;
		}
		public String getTipo() {
			return tipo;
		}
		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		@Override
		public String toString() {
			return "JsonServicio [idservicio=" + idservicio + ", nombre=" + nombre + ", descripcion=" + descripcion
					+ ", costo=" + costo + ", capacidadPersona=" + capacidadPersona + ", tipo=" + tipo + "]";
		}
	    
	    
	    
}
