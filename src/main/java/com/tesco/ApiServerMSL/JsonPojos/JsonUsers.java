package com.tesco.ApiServerMSL.JsonPojos;

public class JsonUsers{
	
	private String usuarioJS;
	private String passJS;
	private String emailJS;
	private String rolesJS;
	
	public JsonUsers(String usuarioJS, String passJS, String emailJS, String rolesJS) {
		super();
		this.usuarioJS = usuarioJS;
		this.passJS = passJS;
		this.emailJS = emailJS;
		this.rolesJS = rolesJS;
	}
	public String getRolesJS() {
		return rolesJS;
	}
	public void setRolesJS(String rolesJS) {
		this.rolesJS = rolesJS;
	}
	public String getUsuarioJS() {
		return usuarioJS;
	}
	public void setUsuarioJS(String usuarioJS) {
		this.usuarioJS = usuarioJS;
	}
	public String getPassJS() {
		return passJS;
	}
	public void setPassJS(String passJS) {
		this.passJS = passJS;
	}
	public String getEmailJS() {
		return emailJS;
	}
	public void setEmailJS(String emailJS) {
		this.emailJS = emailJS;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((emailJS == null) ? 0 : emailJS.hashCode());
		result = prime * result + ((passJS == null) ? 0 : passJS.hashCode());
		result = prime * result + ((rolesJS == null) ? 0 : rolesJS.hashCode());
		result = prime * result + ((usuarioJS == null) ? 0 : usuarioJS.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JsonUsers other = (JsonUsers) obj;
		if (emailJS == null) {
			if (other.emailJS != null)
				return false;
		} else if (!emailJS.equals(other.emailJS))
			return false;
		if (passJS == null) {
			if (other.passJS != null)
				return false;
		} else if (!passJS.equals(other.passJS))
			return false;
		if (rolesJS == null) {
			if (other.rolesJS != null)
				return false;
		} else if (!rolesJS.equals(other.rolesJS))
			return false;
		if (usuarioJS == null) {
			if (other.usuarioJS != null)
				return false;
		} else if (!usuarioJS.equals(other.usuarioJS))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "JsonUsers [usuarioJS=" + usuarioJS + ", passJS=" + passJS + ", emailJS=" + emailJS + ", rolesJS="
				+ rolesJS + "]";
	}
	
	
	
}
