package com.tesco.ApiServerMSL.UImodel;

import javax.validation.Valid;

import com.tesco.ApiServerMSL.model.Reserva;

public class ResSerPer {
	@Valid
	private Reserva reserva;
	private Integer idServicio;
	private String usuario;
	private String fechaIn;
	private String fechaOut;
	private String horaIn;
	private String horaOut;

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	
	public Integer getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFechaIn() {
		return fechaIn;
	}

	public void setFechaIn(String fechaIn) {
		this.fechaIn = fechaIn;
	}

	public String getFechaOut() {
		return fechaOut;
	}

	public void setFechaOut(String fechaOut) {
		this.fechaOut = fechaOut;
	}

	public String getHoraIn() {
		return horaIn;
	}

	public void setHoraIn(String horaIn) {
		this.horaIn = horaIn;
	}

	public String getHoraOut() {
		return horaOut;
	}

	public void setHoraOut(String horaOut) {
		this.horaOut = horaOut;
	}

	@Override
	public String toString() {
		return "ResSerPer [reserva=" + reserva + ", idServicio=" + idServicio + ", usuario=" + usuario + ", fechaIn="
				+ fechaIn + ", fechaOut=" + fechaOut + ", horaIn=" + horaIn + ", horaOut=" + horaOut + "]";
	}
	
	


}
