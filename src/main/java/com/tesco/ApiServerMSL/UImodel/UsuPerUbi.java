package com.tesco.ApiServerMSL.UImodel;

import javax.validation.Valid;

import com.tesco.ApiServerMSL.model.Persona;
import com.tesco.ApiServerMSL.model.Ubicacion;
import com.tesco.ApiServerMSL.model.UsuarioP;

public class UsuPerUbi {
	@Valid
	private UsuarioP usuarioP;
	@Valid
	private Ubicacion ubicacion;
	@Valid
	private Persona persona;
	
	public UsuarioP getUsuarioP() {
		return usuarioP;
	}


	public void setUsuarioP(UsuarioP usuarioP) {
		this.usuarioP = usuarioP;
	}


	public Ubicacion getUbicacion() {
		return ubicacion;
	}


	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}


	public Persona getPersona() {
		return persona;
	}


	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	
	public void setFKusuario() {
		this.persona.setIdUbicacion(this.ubicacion);
		this.persona.setUsuario(this.usuarioP);
	}
}
