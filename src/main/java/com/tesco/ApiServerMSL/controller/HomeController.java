package com.tesco.ApiServerMSL.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tesco.ApiServerMSL.JsonPojos.JsonPagoTargeta;
import com.tesco.ApiServerMSL.UImodel.ResSerPer;
import com.tesco.ApiServerMSL.UImodel.UsuPerUbi;
import com.tesco.ApiServerMSL.model.Factura;
import com.tesco.ApiServerMSL.model.Pago;
import com.tesco.ApiServerMSL.model.Permiso;
import com.tesco.ApiServerMSL.model.Reserva;
import com.tesco.ApiServerMSL.model.Servicio;
import com.tesco.ApiServerMSL.model.TargetaCredito;
import com.tesco.ApiServerMSL.service.FacturaService;
import com.tesco.ApiServerMSL.service.PagoService;
import com.tesco.ApiServerMSL.service.PermisoService;
import com.tesco.ApiServerMSL.service.PersonaService;
import com.tesco.ApiServerMSL.service.ReservaService;
import com.tesco.ApiServerMSL.service.ServicioService;
import com.tesco.ApiServerMSL.service.TargetaCreditoService;
import com.tesco.ApiServerMSL.service.UbicacionService;
import com.tesco.ApiServerMSL.service.UsuarioService;



/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	@Autowired
	private PersonaService personaService;
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private UbicacionService ubicacionService;
	@Autowired
	private PermisoService permisoService;
	@Autowired
	private ServicioService servicioService;
	@Autowired
	private ReservaService reservaService;
	@Autowired
	private FacturaService facturaService;
	@Autowired
	private PagoService pagoService;
	@Autowired
	private TargetaCreditoService targetaCreditoService;

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/web/dfactura/{id}", method = RequestMethod.GET)
	private ModelAndView showFactura(@PathVariable("id") int id,Model model) {
		ModelAndView mav = new ModelAndView("dfactura");
		Factura factura = facturaService.get(id);
		mav.addObject("command", factura);
		return mav;
	}

	@RequestMapping(value = "/web/dcobro", method = RequestMethod.POST)
	public String saveOrUpdateCobro(@ModelAttribute("command") JsonPagoTargeta facturaR, BindingResult result, Model model,
			final RedirectAttributes redirectAttributes) {
		
		if (result.hasErrors()) {
			// esperemos que no reviente
			model.addAttribute("costoTotal", 0);
			model.addAttribute("listOfReservas", null);
			return "dcobro";
		} else {
			System.out.println(facturaR);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Pago pago = null;

			if (facturaR.getNumTargeta() != 0) {
				try {
					targetaCreditoService.save(
							new TargetaCredito(facturaR.getNumTargeta(), sdf.parse(facturaR.getFechaVencimiento()),
									facturaR.getCodSeguridad(), facturaR.getCuotas()));
					redirectAttributes.addFlashAttribute("css", "success");
					redirectAttributes.addFlashAttribute("msg", "Factura registrada correctamente!");
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					redirectAttributes.addFlashAttribute("css", "danger");
					redirectAttributes.addFlashAttribute("msg", "Reserva falla al cargar!");
					e.printStackTrace();
				}
				pago = new Pago(targetaCreditoService.get(facturaR.getNumTargeta()));
				pagoService.save(pago);
			}

			Factura factura;
			try {
				factura = new Factura(facturaR.getMontoTotal(), sdf.parse(facturaR.getFecha()), facturaR.getTipo(),
						pago);
				facturaService.save(factura);
				facturaR.setIdFactura(facturaService.lastFactura());
				
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				if (!(auth instanceof AnonymousAuthenticationToken)) {
					UserDetails userDetail = (UserDetails) auth.getPrincipal();
					System.err.println(userDetail.getUsername());
					List<Reserva> listOfReservasPagas = reservaService.get(userDetail.getUsername(), true);
					for (Reserva reser : listOfReservasPagas) {
						System.err.println(reser.getCantPersona());
						reser.setEstado(null);
						reser.setIdFactura(facturaService.get(facturaR.getIdFactura()));
						reservaService.save(reser);
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return "redirect:/web/dfactura/" + facturaR.getIdFactura();
			//return "redirect:/dcobro";
		}
	}

	// Show Cobro
	@RequestMapping(value = "/web/dcobro", method = RequestMethod.GET)
	public ModelAndView showCobro(Model model) {
		// esta funciona hay que hacerla en el daoImpl y server.
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			List<Reserva> listOfReservas = reservaService.get(userDetail.getUsername(), true);
			float suma = 0;

			if (listOfReservas == null) {
				model.addAttribute("css", "danger");
				model.addAttribute("msg", "User not found");
			} else {
				for (Reserva res : listOfReservas) {
					suma += res.getIdServicio().getCosto();
				}
			}
			model.addAttribute("costoTotal", suma);
			model.addAttribute("listOfReservas", listOfReservas);
		}
		
		return new ModelAndView("dcobro", "command", new JsonPagoTargeta());
	}

	// confirma visita
	@RequestMapping(value = "/web/confirmaRes/{idres}/{user}", method = RequestMethod.GET)
	public String confirmaRes(@PathVariable("idres") int idres, @PathVariable("user") String user,
			HttpServletRequest request, HttpServletResponse response, HttpSession session, Model model,
			final RedirectAttributes redirectAttributes) {
		Reserva reserva = reservaService.get(idres);
		reserva.setEstado(true);
		reservaService.save(reserva);
		// session.setAttribute("listOfReservas", listOfReservas);
		return "redirect:/web/dconfirma";
	}

	// show Confirma
	@RequestMapping(value = "/web/dconfirma", method = RequestMethod.GET)
	public String showConfirma(Model model) {
		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			List<Reserva> listOfReservas = reservaService.get(userDetail.getUsername(), false);
			
			if (listOfReservas == null) {
				System.out.println("es nulo");
				model.addAttribute("css", "danger");
				model.addAttribute("msg", "User not found");
			}else{
				for (Reserva res: listOfReservas){
					System.out.println(res.getFechaIn());
					System.out.println(res.getEstado());
				}
				model.addAttribute("listOfReservas", listOfReservas);
			}
			

		}

		return "dconfirma";

	}

	@RequestMapping(value = "/web/dreservas", method = RequestMethod.POST)
	public String saveOrUpdateReservas(@ModelAttribute("command") @Valid ResSerPer resSerPer, BindingResult result,
			Model model, final RedirectAttributes redirectAttributes) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdt = new SimpleDateFormat("HH:mm");
		System.out.println(resSerPer);
		if (result.hasErrors()) {
			initLists(model);
			return "dreservas";
		} else {
			try {
				Reserva reserva = resSerPer.getReserva();
				reserva.setFechaIn(sdf.parse(resSerPer.getFechaIn()));
				reserva.setFechaOut(sdf.parse(resSerPer.getFechaOut()));
				reserva.setHoraIn(sdt.parse(resSerPer.getHoraIn()));
				reserva.setHoraOut(sdt.parse(resSerPer.getHoraOut()));
				reserva.setDni(personaService.get(resSerPer.getUsuario()));
				reserva.setIdServicio(servicioService.get(resSerPer.getIdServicio()));
				reservaService.save(reserva);
				// Add message to flash scope
				redirectAttributes.addFlashAttribute("css", "success");
				if (resSerPer.getUsuario() == null) {
					redirectAttributes.addFlashAttribute("msg", "Reserva modificado correctamente!");
				} else {
					redirectAttributes.addFlashAttribute("msg", "Reserva registrado correctamente!");
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("msg", "Reserva falla al cargar!");
				e.printStackTrace();
			}

			return "redirect:/web/dreservas";
		}

	}

	void initLists(Model model) {
		Map<Integer, String> serviciosMap = new HashMap<Integer, String>();
		List<Servicio> servicios = servicioService.get();
		for (Servicio ser : servicios) {
			serviciosMap.put(ser.getIdServicio(), ser.getNombre() + " $" + ser.getCosto());
		}
		List<Reserva> listOfReservas = reservaService.get();
		System.out.println(listOfReservas);
		model.addAttribute("serviciosMap", serviciosMap);
		model.addAttribute("listOfReservas", listOfReservas);
	}

	@RequestMapping(value = "/web/dreservas", method = RequestMethod.GET)
	private ModelAndView showReservas(Model model) {
		ModelAndView mav = new ModelAndView("dreservas");
		initLists(model);
		mav.addObject("command", new ResSerPer());
		return mav;
	}

	@RequestMapping(value = "/web/contact", method = RequestMethod.GET)
	public String showContact(Locale locale, Model model) {
		return "contact";
	}

	@RequestMapping(value = "/web/register", method = RequestMethod.GET)
	private ModelAndView showRegister() {
		ModelAndView mav = new ModelAndView("register");
		mav.addObject("command", new UsuPerUbi());
		return mav;
	}

	@RequestMapping(value = "/web/register", method = RequestMethod.POST)
	public String saveOrUpdateUser(@ModelAttribute("command") @Valid UsuPerUbi usuPerUbiL, BindingResult result,
			Model model, final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			System.out.println("error entra");
			return "register";
		} else {
			// Add message to flash scope
			redirectAttributes.addFlashAttribute("css", "success");
			if (usuPerUbiL.getUsuarioP().getUsuario() == null) {
				redirectAttributes.addFlashAttribute("msg", "Usuario modificado correctamente!");
			} else {
				redirectAttributes.addFlashAttribute("msg", "Usuario registrado correctamente!");
			}
			usuPerUbiL.setFKusuario();
			ubicacionService.save(usuPerUbiL.getUbicacion());
			usuarioService.save(usuPerUbiL.getUsuarioP());
			personaService.save(usuPerUbiL.getPersona());
			permisoService.save(new Permiso("ROLE_USER", usuPerUbiL.getUsuarioP()));

			// POST/REDIRECT/GET
			return "redirect:/web/register";

			// POST/FORWARD/GET
			// return "user/list";
		}

	}

	// Spring Security see this :
	@RequestMapping(value = "/web/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
		}

		if (logout != null) {
			model.addObject("msg", "Se ha desconectado correctamente.");
		}
		model.setViewName("login");

		return model;

	}

	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Usuario y contraseña invalido!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "Usuario y contraseña invalido!";
		}

		return error;
	}

	// for 403 access denied page
	@RequestMapping(value = "/web/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);

			model.addObject("username", userDetail.getUsername());

		}

		model.setViewName("403");
		return model;

	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "index";
	}

}
