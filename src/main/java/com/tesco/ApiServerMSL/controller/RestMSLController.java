package com.tesco.ApiServerMSL.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tesco.ApiServerMSL.JsonPojos.JsonEfectivo;
import com.tesco.ApiServerMSL.JsonPojos.JsonFactura;
import com.tesco.ApiServerMSL.JsonPojos.JsonFacturaDato;
import com.tesco.ApiServerMSL.JsonPojos.JsonFacturaMetodoPago;
import com.tesco.ApiServerMSL.JsonPojos.JsonPerson;
import com.tesco.ApiServerMSL.JsonPojos.JsonReserva;
import com.tesco.ApiServerMSL.JsonPojos.JsonReservaPago;
import com.tesco.ApiServerMSL.JsonPojos.JsonServicio;
import com.tesco.ApiServerMSL.JsonPojos.JsonUsers;
import com.tesco.ApiServerMSL.model.Debito;
import com.tesco.ApiServerMSL.model.Efectivo;
import com.tesco.ApiServerMSL.model.Factura;
import com.tesco.ApiServerMSL.model.Pago;
import com.tesco.ApiServerMSL.model.Permiso;
import com.tesco.ApiServerMSL.model.Persona;
import com.tesco.ApiServerMSL.model.Reserva;
import com.tesco.ApiServerMSL.model.Servicio;
import com.tesco.ApiServerMSL.model.TargetaCredito;
import com.tesco.ApiServerMSL.model.Ubicacion;
import com.tesco.ApiServerMSL.model.UsuarioP;
import com.tesco.ApiServerMSL.service.DebitoService;
import com.tesco.ApiServerMSL.service.EfectivoService;
import com.tesco.ApiServerMSL.service.FacturaService;
import com.tesco.ApiServerMSL.service.PagoService;
import com.tesco.ApiServerMSL.service.PermisoService;
import com.tesco.ApiServerMSL.service.PersonaService;
import com.tesco.ApiServerMSL.service.ReservaService;
import com.tesco.ApiServerMSL.service.ServicioService;
import com.tesco.ApiServerMSL.service.TargetaCreditoService;
import com.tesco.ApiServerMSL.service.UbicacionService;
import com.tesco.ApiServerMSL.service.UsuarioService;

@RestController
public class RestMSLController {

	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private PersonaService personaService;
	@Autowired
	private UbicacionService ubicacionService;
	@Autowired
	private ServicioService servicioService;
	@Autowired
	private PermisoService permisoService;
	@Autowired
	private ReservaService reservaService;
	@Autowired
	private FacturaService facturaService;
	@Autowired
	private EfectivoService efectivoService;
	@Autowired
	private PagoService pagoService;
	@Autowired
	private DebitoService debitoService;
	@Autowired
	private TargetaCreditoService targetaCreditoService;

	@RequestMapping(value = "/usuarios", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<JsonUsers> getUsersJson() {
		List<UsuarioP> listOfUsuarios = usuarioService.get();
		List<JsonUsers> listUsers = new ArrayList<JsonUsers>();
		int i;
		int tama = listOfUsuarios.size();
		System.err.println("tamaño " + tama);
		for (i = 0; i < tama; i++) {
			Permiso permisoL = permisoService.get(listOfUsuarios.get(i).getUsuario());
			if (permisoL!=null){
				listUsers.add(new JsonUsers(listOfUsuarios.get(i).getUsuario(), listOfUsuarios.get(i).getPassword(),
						listOfUsuarios.get(i).getEmail(),permisoL.getRoles()));
			}else{
				listUsers.add(new JsonUsers(listOfUsuarios.get(i).getUsuario(), listOfUsuarios.get(i).getPassword(),
						listOfUsuarios.get(i).getEmail(),"OTHER"));
			}
			
		}

		return listUsers;
	}

	@RequestMapping(value = "/persona/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public JsonPerson getPersonaJson(@PathVariable long id) {
		Persona persona = personaService.get(id);
		JsonPerson jsonperson = new JsonPerson(persona.getDni(), persona.getNombre(), persona.getApellido(),
				persona.getTelefono(), persona.getIdUbicacion().getPais(), persona.getIdUbicacion().getProvincia(),
				persona.getIdUbicacion().getCiudad(), persona.getIdUbicacion().getDireccion());

		return jsonperson;
	}

	@RequestMapping(value = "/persona", method = RequestMethod.POST, headers = "Accept=application/json")
	public JsonPerson postAddPersonaJson(@RequestBody JsonPerson personaR) {
		System.out.println(personaR);
		Ubicacion ubicacion = new Ubicacion(personaR.getPais(),personaR.getProvincia(),
		personaR.getCiudad(),personaR.getDireccion());
		Persona persona = new Persona(personaR.getDni(),personaR.getNombre(),personaR.getApellido(),
		personaR.getTelefono(),ubicacion);
		ubicacionService.save(ubicacion);
		personaService.save(persona);
		return personaR;
	}
	
	@RequestMapping(value = "/servicios", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<JsonServicio> getServicioJson() {
		List<Servicio> listOfServicios = servicioService.get();
		List<JsonServicio> listServicios = new ArrayList<JsonServicio>();
		int i;
		int tama = listOfServicios.size();
		for (i = 0; i < tama; i++) {
			listServicios.add(new JsonServicio(listOfServicios.get(i).getIdServicio(),listOfServicios.get(i).getNombre(),
					listOfServicios.get(i).getDescripcion(),listOfServicios.get(i).getCosto(),listOfServicios.get(i).getCapacidadPersona(),
					listOfServicios.get(i).getTipo()));
		}
		
		return listServicios;
	}
	
	@RequestMapping(value = "/reservas", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<JsonReserva> getReservasJson() {
		List<Reserva> listOfReservas = reservaService.get();
		List<JsonReserva> listReservas = new ArrayList<JsonReserva>();
		int i;
		int tama = listOfReservas.size();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdt = new SimpleDateFormat("HH:mm:ss");
		for (i = 0; i < tama; i++) {
			if (listOfReservas.get(i).getIdFactura()!=null){
				listReservas.add(new JsonReserva(listOfReservas.get(i).getIdReserva(),sdf.format(listOfReservas.get(i).getFechaIn())
				,sdf.format(listOfReservas.get(i).getFechaOut()), 
				sdt.format(listOfReservas.get(i).getHoraIn()),
				sdt.format(listOfReservas.get(i).getHoraOut()),
				listOfReservas.get(i).getCantPersona(),String.valueOf(listOfReservas.get(i).getEstado()), 
				listOfReservas.get(i).getDni().getDni(),listOfReservas.get(i).getIdFactura().getIdFactura(),
				listOfReservas.get(i).getIdServicio().getIdServicio() ) );
			}else{
				listReservas.add(new JsonReserva(listOfReservas.get(i).getIdReserva(),sdf.format(listOfReservas.get(i).getFechaIn())
						,sdf.format(listOfReservas.get(i).getFechaOut()), 
						sdt.format(listOfReservas.get(i).getHoraIn()),
						sdt.format(listOfReservas.get(i).getHoraOut()),
						listOfReservas.get(i).getCantPersona(),String.valueOf(listOfReservas.get(i).getEstado()), 
						listOfReservas.get(i).getDni().getDni(),1,
						listOfReservas.get(i).getIdServicio().getIdServicio() ) );
			}
			
		}
		return listReservas;
	}
	
	@RequestMapping(value = "/reservas/{dni}/{est}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<JsonReserva> getReservasJsonDni(@PathVariable long dni,@PathVariable boolean est) {
		List<Reserva> listOfReservas = reservaService.get(dni, est);
		List<JsonReserva> listReservas = new ArrayList<JsonReserva>();
		int i;
		int tama = listOfReservas.size();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdt = new SimpleDateFormat("HH:mm:ss");
		for (i = 0; i < tama; i++) {
			if (listOfReservas.get(i).getIdFactura()!=null){
				listReservas.add(new JsonReserva(listOfReservas.get(i).getIdReserva(),sdf.format(listOfReservas.get(i).getFechaIn())
				,sdf.format(listOfReservas.get(i).getFechaOut()), 
				sdt.format(listOfReservas.get(i).getHoraIn()),
				sdt.format(listOfReservas.get(i).getHoraOut()),
				listOfReservas.get(i).getCantPersona(),String.valueOf(listOfReservas.get(i).getEstado()), 
				listOfReservas.get(i).getDni().getDni(),listOfReservas.get(i).getIdFactura().getIdFactura(),
				listOfReservas.get(i).getIdServicio().getIdServicio() ) );
			}else{
				listReservas.add(new JsonReserva(listOfReservas.get(i).getIdReserva(),sdf.format(listOfReservas.get(i).getFechaIn())
				,sdf.format(listOfReservas.get(i).getFechaOut()), 
				sdt.format(listOfReservas.get(i).getHoraIn()),
				sdt.format(listOfReservas.get(i).getHoraOut()),
				listOfReservas.get(i).getCantPersona(),String.valueOf(listOfReservas.get(i).getEstado()), 
				listOfReservas.get(i).getDni().getDni(),1,
				listOfReservas.get(i).getIdServicio().getIdServicio() ) );
			}
			
		}
		
		return listReservas;
	}
	
	@RequestMapping(value = "/reservas/{dni}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<JsonReservaPago> getReservasJsonDni(@PathVariable long dni) {
		List<Reserva> listOfReservas = reservaService.get(dni, true);
		List<JsonReservaPago> listReservas = new ArrayList<JsonReservaPago>();
		int i;
		int tama = listOfReservas.size();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdt = new SimpleDateFormat("HH:mm:ss");
		for (i = 0; i < tama; i++) {
			listReservas.add(new JsonReservaPago(listOfReservas.get(i).getIdReserva(),sdf.format(listOfReservas.get(i).getFechaIn())
					,sdf.format(listOfReservas.get(i).getFechaOut()), 
					sdt.format(listOfReservas.get(i).getHoraIn()),
					sdt.format(listOfReservas.get(i).getHoraOut()),
					listOfReservas.get(i).getIdServicio().getIdServicio(),
					listOfReservas.get(i).getIdServicio().getNombre(),
					listOfReservas.get(i).getIdServicio().getCosto()
					) );
		}
		
		return listReservas;
	}
	
	@RequestMapping(value = "/reserva", method = RequestMethod.POST, headers = "Accept=application/json")
	public JsonReserva postAddReservaJson(@RequestBody JsonReserva reservaR) {
		System.out.println(reservaR);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdt = new SimpleDateFormat("HH:mm:ss");
		Reserva reserva;
		try {
			reserva = new Reserva(sdf.parse(reservaR.getFechaIn()),sdf.parse(reservaR.getFechaOut()),
					sdt.parse(reservaR.getHoraIn()),sdt.parse(reservaR.getHoraOut()),
					reservaR.getCantPersona(),Boolean.parseBoolean(reservaR.getEstado()),facturaService.get(reservaR.getIdfactura()), personaService.get(reservaR.getDni()),
					servicioService.get(reservaR.getIdServicio()));
			reservaService.save(reserva);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return reservaR;
	}
	
	@RequestMapping(value = "/reservaconfirma/{idres}", method = RequestMethod.GET, headers = "Accept=application/json")
	public void getReservaConJson(@PathVariable int idres) {
		Reserva reserva = reservaService.get(idres);
		reserva.setEstado(true);
		reservaService.save(reserva);
	}
	
	
	
	
	@RequestMapping(value = "/efectivos", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<JsonEfectivo> getEfectivosJson() {
		List<Efectivo> listOfEfectivos = efectivoService.get();
		List<JsonEfectivo> listEfectivos = new ArrayList<JsonEfectivo>();
		int i;
		int tama = listOfEfectivos.size();
		for (i = 0; i < tama; i++) {
			listEfectivos.add(new JsonEfectivo(listOfEfectivos.get(i).getIdEfectivo(),listOfEfectivos.get(i).getMoneda() ) );
		}
		return listEfectivos;
	}
	
	@RequestMapping(value = "/factura", method = RequestMethod.POST, headers = "Accept=application/json")
	public JsonFactura postAddFacturaJson(@RequestBody JsonFactura facturaR) {
		System.out.println(facturaR);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Pago pago=null;
		if (facturaR.getIdEfectivo()!=0){
			pago = new Pago(efectivoService.get(facturaR.getIdEfectivo()));
			pagoService.save(pago);
		}else if(facturaR.getCbu()!=0){
			debitoService.save(new Debito(facturaR.getCbu(),facturaR.getBanco(),facturaR.getTitular()));
			pago = new Pago(debitoService.get(facturaR.getCbu()));
			pagoService.save(pago);
		}else if(facturaR.getNumTargeta()!=0){
			try {
				targetaCreditoService.save(new TargetaCredito(facturaR.getNumTargeta(),
						sdf.parse(facturaR.getFechaVencimiento()),
						facturaR.getCodSeguridad(),facturaR.getCuotas()
						));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pago = new Pago(targetaCreditoService.get(facturaR.getNumTargeta()));
			pagoService.save(pago);
		}
		
		Factura factura;
		try {
			factura = new Factura(
					facturaR.getMontoTotal(),
					sdf.parse(facturaR.getFecha()),
					facturaR.getTipo(),
					pago
					);
			facturaService.save(factura);
			facturaR.setIdFactura(facturaService.lastFactura());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return facturaR;
	}
	
	@RequestMapping(value = "/reservapagada/{idReserva}/{idFactura}", method = RequestMethod.GET, headers = "Accept=application/json")
	public void getReservapagada(@PathVariable int idReserva,@PathVariable int idFactura) {
		Reserva reserva = reservaService.get(idReserva);
		reserva.setEstado(null);
		Factura factura = facturaService.get(idFactura);
		reserva.setIdFactura(factura);
		reservaService.save(reserva);
	}
	
	@RequestMapping(value = "/facturas/{fecha}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<JsonFacturaDato> getGananciaFecha(@PathVariable String fecha) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfP = new SimpleDateFormat("dd-MM-yyyy");
		List<Factura> facturas = facturaService.get(sdfP.parse(fecha));
		System.out.println(facturas);
		List<JsonFacturaDato> jsonFacturas  = new ArrayList<JsonFacturaDato>();
		int i;
		int tama = facturas.size();
		for (i = 0; i < tama; i++) {
			jsonFacturas.add(new JsonFacturaDato(facturas.get(i).getIdFactura(),facturas.get(i).getMontoTotal(),sdf.format(facturas.get(i).getFecha()),facturas.get(i).getTipo() ) );
		}
		return jsonFacturas;
	}
	
	@RequestMapping(value = "/facturas", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<JsonFacturaMetodoPago> getGananciaFecha() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		//SimpleDateFormat sdfP = new SimpleDateFormat("dd-MM-yyyy");
		List<Factura> facturas = facturaService.get();
		System.out.println(facturas);
		List<JsonFacturaMetodoPago> jsonFacturas  = new ArrayList<JsonFacturaMetodoPago>();
		int i;
		int tama = facturas.size();
		for (i = 0; i < tama; i++) {
			if(facturas.get(i).getIdPago().getIdEfectivo()!=null){
				jsonFacturas.add(new JsonFacturaMetodoPago(facturas.get(i).getIdFactura(),facturas.get(i).getMontoTotal(),sdf.format(facturas.get(i).getFecha()),facturas.get(i).getTipo(), 1 ) );
			}else if(facturas.get(i).getIdPago().getCbu()!=null){
				jsonFacturas.add(new JsonFacturaMetodoPago(facturas.get(i).getIdFactura(),facturas.get(i).getMontoTotal(),sdf.format(facturas.get(i).getFecha()),facturas.get(i).getTipo(), 2 ) );
			}else if(facturas.get(i).getIdPago().getNumTargeta()!=null){
				jsonFacturas.add(new JsonFacturaMetodoPago(facturas.get(i).getIdFactura(),facturas.get(i).getMontoTotal(),sdf.format(facturas.get(i).getFecha()),facturas.get(i).getTipo(), 3 ) );
			}else{
				jsonFacturas.add(new JsonFacturaMetodoPago(facturas.get(i).getIdFactura(),facturas.get(i).getMontoTotal(),sdf.format(facturas.get(i).getFecha()),facturas.get(i).getTipo(), 0 ) );
			}	
		}
		return jsonFacturas;
	}
	
	

}
