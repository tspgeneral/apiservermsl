package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Articulo;

public interface ArticuloDao {
	public void save(Articulo articulo);

	public void delete(Articulo articulo);

	public Articulo get(int id);

	public List<Articulo> get();
}
