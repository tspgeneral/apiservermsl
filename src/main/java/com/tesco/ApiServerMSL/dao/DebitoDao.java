package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Debito;

public interface DebitoDao {
	public void save(Debito debito);

	public void delete(Debito debito);

	public Debito get(Long id);

	public List<Debito> get();
}
