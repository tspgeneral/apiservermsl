package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Efectivo;

public interface EfectivoDao {
	public void save(Efectivo efectivo);

	public void delete(Efectivo efectivo);

	public Efectivo get(int id);

	public List<Efectivo> get();
}
