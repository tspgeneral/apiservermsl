package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Empleado;

public interface EmpleadoDao {
	public void save(Empleado empleado);

	public void delete(Empleado empleado);

	public Empleado get(int id);

	public List<Empleado> get();
}
