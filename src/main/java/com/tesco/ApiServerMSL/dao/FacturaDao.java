package com.tesco.ApiServerMSL.dao;

import java.util.Date;
import java.util.List;

import com.tesco.ApiServerMSL.model.Factura;

public interface FacturaDao {
	public void save(Factura factura);

	public void delete(Factura factura);

	public Factura get(int id);

	public List<Factura> get();
	
	public int lastFactura();
	
	public List<Factura> get(Date fecha);
	
}
