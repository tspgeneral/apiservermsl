package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Pago;

public interface PagoDao {
	
	public void save(Pago pago);

	public void delete(Pago pago);

	public Pago get(int id);

	public List<Pago> get();
	
}
	