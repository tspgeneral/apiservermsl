package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Permiso;

public interface PermisoDao {
	
	public void save(Permiso permiso);

	public void delete(Permiso permiso);

	public Permiso get(int id);

	public List<Permiso> get();
	
	public Permiso get(String usuario);
	
}
