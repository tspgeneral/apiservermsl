package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Persona;

public interface PersonaDao {
	public void save(Persona persona);

	public void delete(Persona persona);

	public Persona get(Long id);

	public List<Persona> get();
	
	public Persona get(String usuario);
}
