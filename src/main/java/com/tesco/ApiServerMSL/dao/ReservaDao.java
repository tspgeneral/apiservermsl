package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Reserva;

public interface ReservaDao {
	
	public void save(Reserva reserva);

	public void delete(Reserva reserva);

	public Reserva get(int id);

	public List<Reserva> get();
	
	public List<Reserva> get(long dni, boolean estado);
	
	public List<Reserva> get(String  user, boolean estado);
}
