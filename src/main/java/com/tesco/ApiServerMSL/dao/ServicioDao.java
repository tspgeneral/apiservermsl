package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.Servicio;

public interface ServicioDao {
	
	public void save(Servicio servicio);

	public void delete(Servicio servicio);

	public Servicio get(int id);

	public List<Servicio> get();
}
