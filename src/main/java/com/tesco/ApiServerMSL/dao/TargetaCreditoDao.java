package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.TargetaCredito;

public interface TargetaCreditoDao {
	
	public void save(TargetaCredito targetaCredito);

	public void delete(TargetaCredito targetaCredito);

	public TargetaCredito get(Long id);

	public List<TargetaCredito> get();
	
}
