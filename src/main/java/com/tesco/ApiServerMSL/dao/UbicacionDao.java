package com.tesco.ApiServerMSL.dao;

import java.util.List;


import com.tesco.ApiServerMSL.model.Ubicacion;

public interface UbicacionDao {
	public void save(Ubicacion Ubicacion);

	public void delete(Ubicacion Ubicacion);

	public Ubicacion get(int id);

	public List<Ubicacion> get();
	
}
