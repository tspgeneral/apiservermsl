package com.tesco.ApiServerMSL.dao;

import java.util.List;

import com.tesco.ApiServerMSL.model.UsuarioP;


public interface UsuarioDao {
	
	public void save(UsuarioP usuario);

	public void delete(UsuarioP usuario);

	public UsuarioP get(String id);

	public List<UsuarioP> get();
	
	public UsuarioP findByUserName(String username);
	
}
