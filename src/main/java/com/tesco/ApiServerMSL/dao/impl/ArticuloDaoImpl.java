package com.tesco.ApiServerMSL.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.ArticuloDao;
import com.tesco.ApiServerMSL.model.Articulo;

@Repository
public class ArticuloDaoImpl implements ArticuloDao{
	
	private HibernateTemplate hibernate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	@Override
	public void save(Articulo articulo) {
		hibernate.saveOrUpdate(articulo);
	}
	@Override
	public void delete(Articulo articulo) {
		hibernate.delete(articulo);	
	}
	@Override
	public Articulo get(int id) {
		return hibernate.get(Articulo.class,id);
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<Articulo> get() {
		return (List<Articulo>) hibernate.find("FROM Articulo");
	}
	
}
