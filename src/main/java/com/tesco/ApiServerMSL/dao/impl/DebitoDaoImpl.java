package com.tesco.ApiServerMSL.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.DebitoDao;
import com.tesco.ApiServerMSL.model.Debito;

@Repository
public class DebitoDaoImpl implements DebitoDao{
	
	private HibernateTemplate hibernate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	@Override
	public void save(Debito debito) {
		hibernate.saveOrUpdate(debito);
	}
	@Override
	public void delete(Debito debito) {
		hibernate.delete(debito);	
	}
	@Override
	public Debito get(Long id) {
		return hibernate.get(Debito.class,id);
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<Debito> get() {
		return (List<Debito>) hibernate.find("FROM Debito");
	}

}
