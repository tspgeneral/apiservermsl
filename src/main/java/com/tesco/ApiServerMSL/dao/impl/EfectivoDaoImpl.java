package com.tesco.ApiServerMSL.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.EfectivoDao;
import com.tesco.ApiServerMSL.model.Efectivo;

@Repository
public class EfectivoDaoImpl implements EfectivoDao{
	
private HibernateTemplate hibernate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	@Override
	public void save(Efectivo efectivo) {
		hibernate.saveOrUpdate(efectivo);
	}
	@Override
	public void delete(Efectivo efectivo) {
		hibernate.delete(efectivo);	
	}
	@Override
	public Efectivo get(int id) {
		return hibernate.get(Efectivo.class,id);
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<Efectivo> get() {
		return (List<Efectivo>) hibernate.find("FROM Efectivo");
	}

}
