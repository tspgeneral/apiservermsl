package com.tesco.ApiServerMSL.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.EmpleadoDao;
import com.tesco.ApiServerMSL.model.Empleado;
@Repository
public class EmpleadoDaoImpl implements EmpleadoDao {

	private HibernateTemplate hibernate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	
	@Override
	public void save(Empleado empleado) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(empleado);
	}

	@Override
	public void delete(Empleado empleado) {
		// TODO Auto-generated method stub
		hibernate.delete(empleado);
	}

	@Override
	public Empleado get(int id) {
		// TODO Auto-generated method stub
		return hibernate.get(Empleado.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Empleado> get() {
		// TODO Auto-generated method stub
		return (List<Empleado>) hibernate.find("FROM Empleado");
	}

}
