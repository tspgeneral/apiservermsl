package com.tesco.ApiServerMSL.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.FacturaDao;
import com.tesco.ApiServerMSL.model.Factura;
@Repository
public class FacturaDaoImpl implements FacturaDao{

	private HibernateTemplate hibernate;
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	
	@Override
	public void save(Factura factura) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(factura);
	}

	@Override
	public void delete(Factura factura) {
		// TODO Auto-generated method stub
		hibernate.delete(factura);
	}

	@Override
	public Factura get(int id) {
		// TODO Auto-generated method stub
		return hibernate.get(Factura.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Factura> get() {
		return (List<Factura>) hibernate.find("FROM Factura");
	}

	@Override
	public int lastFactura() {
		int last=0;
		last = (Integer) sessionFactory.getCurrentSession()
				.createQuery("select max(f.idFactura) from Factura f")
				.uniqueResult();
		return last;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Factura> get(Date fecha) {
		List<Factura> facturas = new ArrayList<Factura>();
		
		facturas = sessionFactory.getCurrentSession()
				.createQuery("FROM Factura f WHERE f.fecha=?")
				.setParameter(0 ,fecha)
				.list();


		if (facturas.size() > 0) {
			return facturas;
		} else {
			return null;
		}
	}

}
