package com.tesco.ApiServerMSL.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.PagoDao;
import com.tesco.ApiServerMSL.model.Pago;
@Repository
public class PagoDaoImpl implements PagoDao{

private HibernateTemplate hibernate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	
	@Override
	public void save(Pago pago) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(pago);
	}

	@Override
	public void delete(Pago pago) {
		// TODO Auto-generated method stub
		hibernate.delete(pago);
	}

	@Override
	public Pago get(int id) {
		// TODO Auto-generated method stub
		return hibernate.get(Pago.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Pago> get() {
		// TODO Auto-generated method stub
		return (List<Pago>) hibernate.find("FROM Pago");
	}

}
