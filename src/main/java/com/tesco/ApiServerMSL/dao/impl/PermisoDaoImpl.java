package com.tesco.ApiServerMSL.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.PermisoDao;
import com.tesco.ApiServerMSL.model.Permiso;

@Repository
public class PermisoDaoImpl implements PermisoDao{
	private HibernateTemplate hibernate;
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void save(Permiso permiso) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(permiso);
	}

	@Override
	public void delete(Permiso permiso) {
		// TODO Auto-generated method stub
		hibernate.delete(permiso);
	}

	@Override
	public Permiso get(int id) {
		// TODO Auto-generated method stub
		return hibernate.get(Permiso.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Permiso> get() {
		// TODO Auto-generated method stub
		return (List<Permiso>) hibernate.find("FROM Permiso");
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Permiso get(String usuario) {
		// TODO Auto-generated method stub
		List<Permiso> permi = new ArrayList<Permiso>();
		
		permi = sessionFactory.getCurrentSession()
				.createQuery("FROM Permiso p WHERE p.usuario.usuario=?")
				.setParameter(0, usuario)
				.list();
		if (permi.size() > 0) {
			return permi.get(0);
		} else {
			return null;
		}
		
	}
}
