package com.tesco.ApiServerMSL.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.PersonaDao;
import com.tesco.ApiServerMSL.model.Persona;
@Repository
public class PersonaDaoImpl implements PersonaDao{
	
	private HibernateTemplate hibernate;
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void save(Persona persona) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(persona);
	}

	@Override
	public void delete(Persona persona) {
		// TODO Auto-generated method stub
		hibernate.delete(persona);
	}

	@Override
	public Persona get(Long id) {
		// TODO Auto-generated method stub
		return hibernate.get(Persona.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Persona> get() {
		// TODO Auto-generated method stub
		return (List<Persona>) hibernate.find("FROM Persona");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Persona get(String usuario) {

		List<Persona> personas = new ArrayList<Persona>();

		personas = sessionFactory.getCurrentSession()
			.createQuery("FROM Persona p WHERE p.usuario.usuario=?")
			.setParameter(0, usuario)
			.list();

		if (personas.size() > 0) {
			return personas.get(0);
		} else {
			return null;
		}

	}

}
