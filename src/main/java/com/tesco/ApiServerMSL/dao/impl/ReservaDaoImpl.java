package com.tesco.ApiServerMSL.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.ReservaDao;
import com.tesco.ApiServerMSL.model.Reserva;

@Repository
public class ReservaDaoImpl implements ReservaDao{
	
	private HibernateTemplate hibernate;
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void save(Reserva reserva) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(reserva);
	}

	@Override
	public void delete(Reserva reserva) {
		// TODO Auto-generated method stub
		hibernate.delete(reserva);
	}

	@Override
	public Reserva get(int id) {
		// TODO Auto-generated method stub
		return hibernate.get(Reserva.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Reserva> get() {
		// TODO Auto-generated method stub
		return (List<Reserva>) hibernate.find("FROM Reserva");
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<Reserva> get(long dni, boolean estado) {

		List<Reserva> reservas = new ArrayList<Reserva>();

		reservas = sessionFactory.getCurrentSession()
			.createQuery("FROM Reserva r WHERE r.estado=? and r.dni.dni=?")
			.setParameter(0, estado)
			.setParameter(1, dni)
			.list();

		if (reservas.size() > 0) {
			return reservas;
		} else {
			return null;
		}

	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Reserva> get(String user, boolean estado) {
		
		List<Reserva> reservas = new ArrayList<Reserva>();

		reservas = sessionFactory.getCurrentSession()
			.createQuery("FROM Reserva r WHERE r.estado=? and r.dni.usuario.usuario=?")
			.setParameter(0, estado)
			.setParameter(1, user)
			.list();

		if (reservas.size() > 0) {
			return reservas;
		} else {
			return null;
		}
	}

}
