package com.tesco.ApiServerMSL.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.ServicioDao;
import com.tesco.ApiServerMSL.model.Servicio;
@Repository
public class ServicioDaoImpl implements ServicioDao{
	
private HibernateTemplate hibernate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	
	@Override
	public void save(Servicio servicio) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(servicio);
	}

	@Override
	public void delete(Servicio servicio) {
		// TODO Auto-generated method stub
		hibernate.delete(servicio);
	}

	@Override
	public Servicio get(int id) {
		// TODO Auto-generated method stub
		return hibernate.get(Servicio.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Servicio> get() {
		// TODO Auto-generated method stub
		return (List<Servicio>) hibernate.find("FROM Servicio");
	}
	

}
