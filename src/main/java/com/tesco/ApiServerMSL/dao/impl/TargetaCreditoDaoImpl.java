package com.tesco.ApiServerMSL.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.TargetaCreditoDao;
import com.tesco.ApiServerMSL.model.TargetaCredito;
@Repository
public class TargetaCreditoDaoImpl implements TargetaCreditoDao{
	private HibernateTemplate hibernate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	
	@Override
	public void save(TargetaCredito targetaCredito) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(targetaCredito);
	}

	@Override
	public void delete(TargetaCredito targetaCredito) {
		// TODO Auto-generated method stub
		hibernate.delete(targetaCredito);
	}

	@Override
	public TargetaCredito get(Long id) {
		// TODO Auto-generated method stub
		return hibernate.get(TargetaCredito.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TargetaCredito> get() {
		// TODO Auto-generated method stub
		return (List<TargetaCredito>) hibernate.find("FROM TargetaCredito");
	}

}
