package com.tesco.ApiServerMSL.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.UbicacionDao;
import com.tesco.ApiServerMSL.model.Ubicacion;
@Repository
public class UbicacionDaoImpl implements UbicacionDao{
	
	private HibernateTemplate hibernate;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}

	@Override
	public void save(Ubicacion Ubicacion) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(Ubicacion);
	}

	@Override
	public void delete(Ubicacion Ubicacion) {
		// TODO Auto-generated method stub
		hibernate.delete(Ubicacion);
	}

	@Override
	public Ubicacion get(int id) {
		// TODO Auto-generated method stub
		return hibernate.get(Ubicacion.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Ubicacion> get() {
		// TODO Auto-generated method stub
		return (List<Ubicacion>) hibernate.find("FROM Ubicacion");
	}
}
