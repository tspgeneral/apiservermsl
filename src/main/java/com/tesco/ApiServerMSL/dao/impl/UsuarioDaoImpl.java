package com.tesco.ApiServerMSL.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.tesco.ApiServerMSL.dao.UsuarioDao;
import com.tesco.ApiServerMSL.model.UsuarioP;


@Repository
public class UsuarioDaoImpl implements UsuarioDao {
	
	private HibernateTemplate hibernate;
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory){
		this.hibernate = new HibernateTemplate(sessionFactory);
	}
	@Override
	public void save(UsuarioP usuario) {
		// TODO Auto-generated method stub
		hibernate.saveOrUpdate(usuario);
	}

	@Override
	public void delete(UsuarioP usuario) {
		// TODO Auto-generated method stub
		hibernate.delete(usuario);
	}

	@Override
	public UsuarioP get(String id) {
		// TODO Auto-generated method stub
		return hibernate.get(UsuarioP.class,id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<UsuarioP> get() {
		// TODO Auto-generated method stub
		return (List<UsuarioP>) hibernate.find("FROM UsuarioP");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public UsuarioP findByUserName(String username) {

		List<UsuarioP> users = new ArrayList<UsuarioP>();

		users = sessionFactory.getCurrentSession()
			.createQuery("from UsuarioP where usuario=?")
			.setParameter(0, username)
			.list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}

}
