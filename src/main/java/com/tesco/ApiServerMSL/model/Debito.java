/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesco.ApiServerMSL.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author tesco01
 */
@Entity
@Table(name = "Debito")
@NamedQueries({
    @NamedQuery(name = "Debito.findAll", query = "SELECT d FROM Debito d"),
    @NamedQuery(name = "Debito.findByCbu", query = "SELECT d FROM Debito d WHERE d.cbu = :cbu"),
    @NamedQuery(name = "Debito.findByBanco", query = "SELECT d FROM Debito d WHERE d.banco = :banco"),
    @NamedQuery(name = "Debito.findByTitular", query = "SELECT d FROM Debito d WHERE d.titular = :titular")})
public class Debito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CBU")
    private Long cbu;
    @Column(name = "banco")
    private String banco;
    @Column(name = "titular")
    private String titular;
    @OneToMany(mappedBy = "cbu", fetch = FetchType.EAGER)
    private Set<Pago> pagoSet;

    public Debito() {
    }
    
    public Debito(Long cbu) {
        this.cbu = cbu;
    }
    
    

    public Debito(Long cbu, String banco, String titular) {
		super();
		this.cbu = cbu;
		this.banco = banco;
		this.titular = titular;
	}

	public Long getCbu() {
        return cbu;
    }

    public void setCbu(Long cbu) {
        this.cbu = cbu;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public Set<Pago> getPagoSet() {
        return pagoSet;
    }

    public void setPagoSet(Set<Pago> pagoSet) {
        this.pagoSet = pagoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cbu != null ? cbu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Debito)) {
            return false;
        }
        Debito other = (Debito) object;
        if ((this.cbu == null && other.cbu != null) || (this.cbu != null && !this.cbu.equals(other.cbu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "crmce.model.Debito[ cbu=" + cbu + " ]";
    }
    
}
