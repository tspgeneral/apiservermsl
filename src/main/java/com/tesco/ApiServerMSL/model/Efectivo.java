/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesco.ApiServerMSL.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author tesco01
 */
@Entity
@Table(name = "Efectivo")
@NamedQueries({
    @NamedQuery(name = "Efectivo.findAll", query = "SELECT e FROM Efectivo e"),
    @NamedQuery(name = "Efectivo.findByIdEfectivo", query = "SELECT e FROM Efectivo e WHERE e.idEfectivo = :idEfectivo"),
    @NamedQuery(name = "Efectivo.findByMoneda", query = "SELECT e FROM Efectivo e WHERE e.moneda = :moneda")})
public class Efectivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEfectivo")
    private Integer idEfectivo;
    @Column(name = "moneda")
    private String moneda;
    @OneToMany(mappedBy = "idEfectivo", fetch = FetchType.EAGER)
    private Set<Pago> pagoSet;

    public Efectivo() {
    }

    public Efectivo(Integer idEfectivo) {
        this.idEfectivo = idEfectivo;
    }

    public Integer getIdEfectivo() {
        return idEfectivo;
    }

    public void setIdEfectivo(Integer idEfectivo) {
        this.idEfectivo = idEfectivo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Set<Pago> getPagoSet() {
        return pagoSet;
    }

    public void setPagoSet(Set<Pago> pagoSet) {
        this.pagoSet = pagoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEfectivo != null ? idEfectivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Efectivo)) {
            return false;
        }
        Efectivo other = (Efectivo) object;
        if ((this.idEfectivo == null && other.idEfectivo != null) || (this.idEfectivo != null && !this.idEfectivo.equals(other.idEfectivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "crmce.model.Efectivo[ idEfectivo=" + idEfectivo + " ]";
    }
    
}
