/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesco.ApiServerMSL.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author tesco01
 */
@Entity
@Table(name = "Empleado")
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e"),
    @NamedQuery(name = "Empleado.findByLegajo", query = "SELECT e FROM Empleado e WHERE e.legajo = :legajo"),
    @NamedQuery(name = "Empleado.findByPuesto", query = "SELECT e FROM Empleado e WHERE e.puesto = :puesto"),
    @NamedQuery(name = "Empleado.findBySitio", query = "SELECT e FROM Empleado e WHERE e.sitio = :sitio")})
public class Empleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "legajo")
    private Integer legajo;
    @Basic(optional = false)
    @Column(name = "puesto")
    private String puesto;
    @Column(name = "sitio")
    private String sitio;
    @JoinColumn(name = "DNI", referencedColumnName = "DNI")
    @ManyToOne(fetch = FetchType.EAGER)
    private Persona dni;

    public Empleado() {
    }

    public Empleado(Integer legajo) {
        this.legajo = legajo;
    }

    public Empleado(Integer legajo, String puesto) {
        this.legajo = legajo;
        this.puesto = puesto;
    }

    public Integer getLegajo() {
        return legajo;
    }

    public void setLegajo(Integer legajo) {
        this.legajo = legajo;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public Persona getDni() {
        return dni;
    }

    public void setDni(Persona dni) {
        this.dni = dni;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (legajo != null ? legajo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.legajo == null && other.legajo != null) || (this.legajo != null && !this.legajo.equals(other.legajo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "crmce.model.Empleado[ legajo=" + legajo + " ]";
    }
    
}
