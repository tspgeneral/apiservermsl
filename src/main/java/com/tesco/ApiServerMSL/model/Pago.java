/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesco.ApiServerMSL.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author tesco01
 */
@Entity
@Table(name = "Pago")
@NamedQueries({
    @NamedQuery(name = "Pago.findAll", query = "SELECT p FROM Pago p"),
    @NamedQuery(name = "Pago.findByIdPago", query = "SELECT p FROM Pago p WHERE p.idPago = :idPago")})
public class Pago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPago")
    private Integer idPago;
    @OneToMany(mappedBy = "idPago", fetch = FetchType.EAGER)
    private Set<Factura> facturaSet;
    @JoinColumn(name = "CBU", referencedColumnName = "CBU")
    @ManyToOne(fetch = FetchType.EAGER)
    private Debito cbu;
    @JoinColumn(name = "idEfectivo", referencedColumnName = "idEfectivo")
    @ManyToOne(fetch = FetchType.EAGER)
    private Efectivo idEfectivo;
    @JoinColumn(name = "numTargeta", referencedColumnName = "numTargeta")
    @ManyToOne(fetch = FetchType.EAGER)
    private TargetaCredito numTargeta;

    public Pago() {
    }    

    public Pago(Efectivo idEfectivo) {
		super();
		this.idEfectivo = idEfectivo;
	}
    
	public Pago(Debito cbu) {
		super();
		this.cbu = cbu;
	}

	public Pago(TargetaCredito numTargeta) {
		super();
		this.numTargeta = numTargeta;
	}

	public Integer getIdPago() {
        return idPago;
    }

    public void setIdPago(Integer idPago) {
        this.idPago = idPago;
    }

    public Set<Factura> getFacturaSet() {
        return facturaSet;
    }

    public void setFacturaSet(Set<Factura> facturaSet) {
        this.facturaSet = facturaSet;
    }

    public Debito getCbu() {
        return cbu;
    }

    public void setCbu(Debito cbu) {
        this.cbu = cbu;
    }

    public Efectivo getIdEfectivo() {
        return idEfectivo;
    }

    public void setIdEfectivo(Efectivo idEfectivo) {
        this.idEfectivo = idEfectivo;
    }

    public TargetaCredito getNumTargeta() {
        return numTargeta;
    }

    public void setNumTargeta(TargetaCredito numTargeta) {
        this.numTargeta = numTargeta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPago != null ? idPago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pago)) {
            return false;
        }
        Pago other = (Pago) object;
        if ((this.idPago == null && other.idPago != null) || (this.idPago != null && !this.idPago.equals(other.idPago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "crmce.model.Pago[ idPago=" + idPago + " ]";
    }
    
}
