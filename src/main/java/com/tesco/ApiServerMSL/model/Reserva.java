/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesco.ApiServerMSL.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tesco01
 */
@Entity
@Table(name = "Reserva")
@NamedQueries({
    @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r"),
    @NamedQuery(name = "Reserva.findByIdReserva", query = "SELECT r FROM Reserva r WHERE r.idReserva = :idReserva"),
    @NamedQuery(name = "Reserva.findByFechaIn", query = "SELECT r FROM Reserva r WHERE r.fechaIn = :fechaIn"),
    @NamedQuery(name = "Reserva.findByFechaOut", query = "SELECT r FROM Reserva r WHERE r.fechaOut = :fechaOut"),
    @NamedQuery(name = "Reserva.findByHoraIn", query = "SELECT r FROM Reserva r WHERE r.horaIn = :horaIn"),
    @NamedQuery(name = "Reserva.findByHoraOut", query = "SELECT r FROM Reserva r WHERE r.horaOut = :horaOut"),
    @NamedQuery(name = "Reserva.findByCantPersona", query = "SELECT r FROM Reserva r WHERE r.cantPersona = :cantPersona")})
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReserva")
    private Integer idReserva;
    @Column(name = "fechaIn")
    @Temporal(TemporalType.DATE)
    private Date fechaIn;
    @Column(name = "fechaOut")
    @Temporal(TemporalType.DATE)
    private Date fechaOut;
    @Column(name = "horaIn")
    @Temporal(TemporalType.TIME)
    private Date horaIn;
    @Column(name = "horaOut")
    @Temporal(TemporalType.TIME)
    private Date horaOut;
    @Column(name = "cantPersona")
    private Integer cantPersona;
    @Column(name = "estado")
    private Boolean estado; 
    @JoinColumn(name = "idFactura", referencedColumnName = "idFactura")
    @ManyToOne(fetch = FetchType.EAGER)
    private Factura idFactura;
    @JoinColumn(name = "DNI", referencedColumnName = "DNI")
    @ManyToOne(fetch = FetchType.EAGER)
    private Persona dni;
    @JoinColumn(name = "idServicio", referencedColumnName = "idServicio")
    @ManyToOne(fetch = FetchType.EAGER)
    private Servicio idServicio;

    public Reserva() {
    }
    

    public Reserva(Integer idReserva, Date fechaIn, Date fechaOut, Date horaIn, Date horaOut, Integer cantPersona,
			Boolean estado, Persona dni, Servicio idServicio) {
		super();
		this.idReserva = idReserva;
		this.fechaIn = fechaIn;
		this.fechaOut = fechaOut;
		this.horaIn = horaIn;
		this.horaOut = horaOut;
		this.cantPersona = cantPersona;
		this.estado = estado;
		this.dni = dni;
		this.idServicio = idServicio;
	}



	public Reserva(Date fechaIn, Date fechaOut, Date horaIn, Date horaOut, Integer cantPersona, Boolean estado,
			Factura idFactura, Persona dni, Servicio idServicio) {
		super();
		this.fechaIn = fechaIn;
		this.fechaOut = fechaOut;
		this.horaIn = horaIn;
		this.horaOut = horaOut;
		this.cantPersona = cantPersona;
		this.estado = estado;
		this.idFactura = idFactura;
		this.dni = dni;
		this.idServicio = idServicio;
	}



	public Reserva(Integer idReserva) {
        this.idReserva = idReserva;
    }
    
    public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public Date getFechaIn() {
        return fechaIn;
    }

    public void setFechaIn(Date fechaIn) {
        this.fechaIn = fechaIn;
    }

    public Date getFechaOut() {
        return fechaOut;
    }

    public void setFechaOut(Date fechaOut) {
        this.fechaOut = fechaOut;
    }

    public Date getHoraIn() {
        return horaIn;
    }

    public void setHoraIn(Date horaIn) {
        this.horaIn = horaIn;
    }

    public Date getHoraOut() {
        return horaOut;
    }

    public void setHoraOut(Date horaOut) {
        this.horaOut = horaOut;
    }

    public Integer getCantPersona() {
        return cantPersona;
    }

    public void setCantPersona(Integer cantPersona) {
        this.cantPersona = cantPersona;
    }

    public Factura getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Factura idFactura) {
        this.idFactura = idFactura;
    }
    
    public Persona getDni() {
        return dni;
    }

    public void setDni(Persona dni) {
        this.dni = dni;
    }

    public Servicio getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Servicio idServicio) {
        this.idServicio = idServicio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReserva != null ? idReserva.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reserva)) {
            return false;
        }
        Reserva other = (Reserva) object;
        if ((this.idReserva == null && other.idReserva != null) || (this.idReserva != null && !this.idReserva.equals(other.idReserva))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "crmce.model.Reserva[ idReserva=" + idReserva + " ]";
    }
    
}
