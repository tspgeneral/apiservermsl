/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesco.ApiServerMSL.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author tesco01
 */
@Entity
@Table(name = "TargetaCredito")
@NamedQueries({
    @NamedQuery(name = "TargetaCredito.findAll", query = "SELECT t FROM TargetaCredito t"),
    @NamedQuery(name = "TargetaCredito.findByNumTargeta", query = "SELECT t FROM TargetaCredito t WHERE t.numTargeta = :numTargeta"),
    @NamedQuery(name = "TargetaCredito.findByFechaVencimiento", query = "SELECT t FROM TargetaCredito t WHERE t.fechaVencimiento = :fechaVencimiento"),
    @NamedQuery(name = "TargetaCredito.findByCodSeguridad", query = "SELECT t FROM TargetaCredito t WHERE t.codSeguridad = :codSeguridad"),
    @NamedQuery(name = "TargetaCredito.findByCuotas", query = "SELECT t FROM TargetaCredito t WHERE t.cuotas = :cuotas")})
public class TargetaCredito implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "numTargeta")
    private Long numTargeta;
    @Column(name = "fechaVencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @Column(name = "codSeguridad")
    private Integer codSeguridad;
    @Column(name = "cuotas")
    private Integer cuotas;
    @OneToMany(mappedBy = "numTargeta", fetch = FetchType.EAGER)
    private Set<Pago> pagoSet;

    public TargetaCredito() {
    }
    

    public TargetaCredito(Long numTargeta, Date fechaVencimiento, Integer codSeguridad, Integer cuotas) {
		super();
		this.numTargeta = numTargeta;
		this.fechaVencimiento = fechaVencimiento;
		this.codSeguridad = codSeguridad;
		this.cuotas = cuotas;
	}



	public TargetaCredito(Long numTargeta) {
        this.numTargeta = numTargeta;
    }

    public Long getNumTargeta() {
        return numTargeta;
    }

    public void setNumTargeta(Long numTargeta) {
        this.numTargeta = numTargeta;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Integer getCodSeguridad() {
        return codSeguridad;
    }

    public void setCodSeguridad(Integer codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public Integer getCuotas() {
        return cuotas;
    }

    public void setCuotas(Integer cuotas) {
        this.cuotas = cuotas;
    }

    public Set<Pago> getPagoSet() {
        return pagoSet;
    }

    public void setPagoSet(Set<Pago> pagoSet) {
        this.pagoSet = pagoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numTargeta != null ? numTargeta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TargetaCredito)) {
            return false;
        }
        TargetaCredito other = (TargetaCredito) object;
        if ((this.numTargeta == null && other.numTargeta != null) || (this.numTargeta != null && !this.numTargeta.equals(other.numTargeta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "crmce.model.TargetaCredito[ numTargeta=" + numTargeta + " ]";
    }
    
}
