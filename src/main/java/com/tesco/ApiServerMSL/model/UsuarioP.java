/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tesco.ApiServerMSL.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author tesco01
 */
@Entity
@Table(name = "UsuarioP")
@NamedQueries({
    @NamedQuery(name = "UsuarioP.findAll", query = "SELECT u FROM UsuarioP u"),
    @NamedQuery(name = "UsuarioP.findByUsuario", query = "SELECT u FROM UsuarioP u WHERE u.usuario = :usuario"),
    @NamedQuery(name = "UsuarioP.findByPassword", query = "SELECT u FROM UsuarioP u WHERE u.password = :password"),
    @NamedQuery(name = "UsuarioP.findByEmail", query = "SELECT u FROM UsuarioP u WHERE u.email = :email")})
public class UsuarioP implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "usuario")
    private String usuario;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;
    @OneToMany(mappedBy = "usuario", fetch = FetchType.EAGER)
    private Set<Persona> personaSet;
    @OneToMany(mappedBy = "usuario", fetch = FetchType.EAGER)
    private Set<Permiso> permisoSet;

    public UsuarioP() {
    }

    public UsuarioP(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Persona> getPersonaSet() {
        return personaSet;
    }

    public void setPersonaSet(Set<Persona> personaSet) {
        this.personaSet = personaSet;
    }

    public Set<Permiso> getPermisoSet() {
        return permisoSet;
    }

    public void setPermisoSet(Set<Permiso> permisoSet) {
        this.permisoSet = permisoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuario != null ? usuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioP)) {
            return false;
        }
        UsuarioP other = (UsuarioP) object;
        if ((this.usuario == null && other.usuario != null) || (this.usuario != null && !this.usuario.equals(other.usuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "crmce.model.UsuarioP[ usuario=" + usuario + " ]";
    }
    
}
