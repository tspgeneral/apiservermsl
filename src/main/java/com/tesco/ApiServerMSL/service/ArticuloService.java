package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Articulo;

public interface ArticuloService {
	
	public void save(Articulo articulo);

	public void delete(int id);

	public Articulo get(int id);

	public List<Articulo> get();

}
