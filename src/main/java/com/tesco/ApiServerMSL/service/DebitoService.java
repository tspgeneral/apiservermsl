package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Debito;

public interface DebitoService {
	
	public void save(Debito debito);

	public void delete(Long id);

	public Debito get(Long id);

	public List<Debito> get();

}
