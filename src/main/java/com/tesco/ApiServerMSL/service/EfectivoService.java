package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Efectivo;

public interface EfectivoService {
	
	public void save(Efectivo efectivo);

	public void delete(int id);

	public Efectivo get(int id);

	public List<Efectivo> get();

}
