package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Empleado;

public interface EmpleadoService {
	
	public void save(Empleado empleado);

	public void delete(int id);

	public Empleado get(int id);

	public List<Empleado> get();

}
