package com.tesco.ApiServerMSL.service;

import java.util.Date;
import java.util.List;

import com.tesco.ApiServerMSL.model.Factura;

public interface FacturaService {
	
	public void save(Factura factura);

	public void delete(int id);

	public Factura get(int id);

	public List<Factura> get();
	
	public int lastFactura();
	
	public List<Factura> get(Date fecha); 

}
