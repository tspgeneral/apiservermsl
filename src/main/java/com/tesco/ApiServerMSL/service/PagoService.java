package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Pago;

public interface PagoService {

	public void save(Pago pago);

	public void delete(int id);

	public Pago get(int id);

	public List<Pago> get();
	
}
