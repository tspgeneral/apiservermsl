package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Permiso;

public interface PermisoService {
	
	public void save(Permiso permiso);

	public void delete(int id);

	public Permiso get(int id);

	public List<Permiso> get();
	
	public Permiso get(String usuario);

}
