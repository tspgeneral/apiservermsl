package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Persona;

public interface PersonaService {
	
	public void save(Persona persona);

	public void delete(Long id);

	public Persona get(Long id);

	public List<Persona> get();
	
	public Persona get(String usuario);

}
