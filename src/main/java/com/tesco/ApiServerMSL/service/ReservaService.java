package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Reserva;

public interface ReservaService {
	
	public void save(Reserva reserva);

	public void delete(int id);

	public Reserva get(int id);

	public List<Reserva> get();
	
	public List<Reserva> get(long dni, boolean estado);
	
	public List<Reserva> get(String user, boolean estado);

}
