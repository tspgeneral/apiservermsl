package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Servicio;

public interface ServicioService {

	public void save(Servicio servicio);

	public void delete(int id);

	public Servicio get(int id);

	public List<Servicio> get();
	
}
