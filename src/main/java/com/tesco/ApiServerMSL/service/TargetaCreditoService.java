package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.TargetaCredito;

public interface TargetaCreditoService {
	public void save(TargetaCredito targetaCredito);

	public void delete(Long id);

	public TargetaCredito get(Long id);

	public List<TargetaCredito> get();
}
