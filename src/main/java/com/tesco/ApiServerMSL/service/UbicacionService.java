package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.Ubicacion;

public interface UbicacionService {
	
	public void save(Ubicacion ubicacion);

	public void delete(int id);

	public Ubicacion get(int id);

	public List<Ubicacion> get();
	
}
