package com.tesco.ApiServerMSL.service;

import java.util.List;

import com.tesco.ApiServerMSL.model.UsuarioP;

public interface UsuarioService {
	
	public void save(UsuarioP usuario);

	public void delete(String id);

	public UsuarioP get(String id);

	public List<UsuarioP> get();
	
	public UsuarioP findByUserName(String username);
	
}
