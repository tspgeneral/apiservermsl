package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.ArticuloDao;
import com.tesco.ApiServerMSL.model.Articulo;
import com.tesco.ApiServerMSL.service.ArticuloService;

@Service
public class ArticuloServiceImpl implements ArticuloService {

	@Resource(name = "ArticuloDao")
	private ArticuloDao dao;
	
	@Override
	@Transactional
	public void save(Articulo articulo) {
		// TODO Auto-generated method stub
		dao.save(articulo);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Articulo articulo = this.get(id);
		if(articulo!=null){
			dao.delete(articulo);
		}
	}

	@Override
	@Transactional
	public Articulo get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Articulo> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}

}
