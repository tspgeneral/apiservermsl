package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.DebitoDao;
import com.tesco.ApiServerMSL.model.Debito;
import com.tesco.ApiServerMSL.service.DebitoService;

@Service
public class DebitoServiceImpl implements DebitoService{

	@Resource(name = "DebitoDao")
	private DebitoDao dao;
	
	@Override
	@Transactional
	public void save(Debito debito) {
		// TODO Auto-generated method stub
		dao.save(debito);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		Debito debito = this.get(id);
		if(debito!=null){
			dao.delete(debito);
		}
	}

	@Override
	@Transactional
	public Debito get(Long id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Debito> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}

}
