package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tesco.ApiServerMSL.dao.EfectivoDao;
import com.tesco.ApiServerMSL.model.Efectivo;
import com.tesco.ApiServerMSL.service.EfectivoService;

@Service
public class EfectivoServiceImpl implements EfectivoService{

	@Resource(name = "EfectivoDao")
	private EfectivoDao dao;
	
	@Override
	public void save(Efectivo efectivo) {
		// TODO Auto-generated method stub
		dao.save(efectivo);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Efectivo efectivo = this.get(id);
		if(efectivo!=null){
			dao.delete(efectivo);
		}
	}

	@Override
	public Efectivo get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	public List<Efectivo> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}

}
