package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.EmpleadoDao;
import com.tesco.ApiServerMSL.model.Empleado;
import com.tesco.ApiServerMSL.service.EmpleadoService;

@Service
public class EmpleadoServiceImpl implements EmpleadoService{

	@Resource(name = "EmpleadoDao")
	private EmpleadoDao dao;
	
	@Override
	@Transactional
	public void save(Empleado empleado) {
		// TODO Auto-generated method stub
		dao.save(empleado);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Empleado empleado = this.get(id);
		if(empleado!=null){
			dao.delete(empleado);
		}
	}

	@Override
	@Transactional
	public Empleado get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Empleado> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}

}
