package com.tesco.ApiServerMSL.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.FacturaDao;
import com.tesco.ApiServerMSL.model.Factura;
import com.tesco.ApiServerMSL.service.FacturaService;

@Service
public class FacturaServiceImpl implements FacturaService{

	@Resource(name = "FacturaDao")
	private FacturaDao dao;
	
	@Override
	@Transactional
	public void save(Factura factura) {
		// TODO Auto-generated method stub
		dao.save(factura);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Factura factura = this.get(id);
		if(factura!=null){
			dao.delete(factura);
		}
	}

	@Override
	@Transactional
	public Factura get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Factura> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}

	@Override
	@Transactional
	public int lastFactura() {
		// TODO Auto-generated method stub
		return dao.lastFactura();
	}

	@Override
	@Transactional
	public List<Factura> get(Date fecha) {
		// TODO Auto-generated method stub
		return dao.get(fecha);
	}

}
