package com.tesco.ApiServerMSL.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.UsuarioDao;
import com.tesco.ApiServerMSL.model.Permiso;
import com.tesco.ApiServerMSL.model.UsuarioP;


@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {
	
	@Resource(name = "UsuarioDao")
	private UsuarioDao dao;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException , DataAccessException {
		    System.out.println("beto  :" + username);
			try {
				if (username == null){ throw new UsernameNotFoundException("User not found" + username);}
				UsuarioP userP = dao.findByUserName(username);
				//System.err.println(userP.getEmail());
				List<GrantedAuthority> authorities = buildUserAuthority(userP.getPermisoSet());
				return buildUserForAuthentication(userP, authorities);
			} catch (Exception e) {
				e.getMessage();
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		
			
	}

	// Converts com.mkyong.users.model.User user to
	// org.springframework.security.core.userdetails.User
	private User buildUserForAuthentication(UsuarioP userp, List<GrantedAuthority> authorities) {
		return new User(userp.getUsuario(), userp.getPassword(), true, true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(Set<Permiso> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (Permiso userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRoles()));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}

}