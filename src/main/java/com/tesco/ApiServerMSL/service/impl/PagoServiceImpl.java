package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.PagoDao;
import com.tesco.ApiServerMSL.model.Pago;
import com.tesco.ApiServerMSL.service.PagoService;

@Service
public class PagoServiceImpl implements PagoService{
	
	@Resource(name = "PagoDao")
	private PagoDao dao;

	@Override
	@Transactional
	public void save(Pago pago) {
		// TODO Auto-generated method stub
		dao.save(pago);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Pago pago = this.get(id);
		if(pago!=null){
			dao.delete(pago);
		}
	}

	@Override
	@Transactional
	public Pago get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Pago> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}
	
	

}
