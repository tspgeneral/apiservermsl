package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.PermisoDao;
import com.tesco.ApiServerMSL.model.Permiso;
import com.tesco.ApiServerMSL.service.PermisoService;
@Service
public class PermisoServiceImpl implements PermisoService{

	@Resource(name = "PermisoDao")
	private PermisoDao dao;
	
	@Override
	@Transactional
	public void save(Permiso permiso) {
		// TODO Auto-generated method stub
		dao.save(permiso);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Permiso permiso = this.get(id);
		if(permiso!=null){
			dao.delete(permiso);
		}
	}

	@Override
	@Transactional
	public Permiso get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Permiso> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}
	
	@Override
	@Transactional
	public Permiso get(String usuario)
	{
		// TODO Auto-generated method stub
		return dao.get(usuario);
	}

}
