package com.tesco.ApiServerMSL.service.impl;

import java.util.List;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.PersonaDao;
import com.tesco.ApiServerMSL.model.Persona;
import com.tesco.ApiServerMSL.service.PersonaService;
@Service
public class PersonaServiceImpl implements PersonaService{

	@Resource(name = "PersonaDao")
	private PersonaDao dao;
	
	@Override
	@Transactional
	public void save(Persona persona) {
		// TODO Auto-generated method stub
		dao.save(persona);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		Persona persona = this.get(id);
		if(persona!=null){
			dao.delete(persona);
		}
	}

	@Override
	@Transactional
	public Persona get(Long id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Persona> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}

	@Override
	@Transactional
	public Persona get(String usuario) {
		// TODO Auto-generated method stub
		return dao.get(usuario);
	}

}
