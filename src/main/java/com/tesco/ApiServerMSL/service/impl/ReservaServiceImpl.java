package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.ReservaDao;
import com.tesco.ApiServerMSL.model.Reserva;
import com.tesco.ApiServerMSL.service.ReservaService;

@Service
public class ReservaServiceImpl implements ReservaService{
	
	@Resource(name = "ReservaDao")
	private ReservaDao dao;
	
	@Override
	@Transactional
	public void save(Reserva reserva) {
		// TODO Auto-generated method stub
		dao.save(reserva);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Reserva reserva = this.get(id);
		if(reserva!=null){
			dao.delete(reserva);
		}
	}

	@Override
	@Transactional
	public Reserva get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Reserva> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}
	
	@Override
	@Transactional
	public List<Reserva> get(long dni, boolean estado){
		return dao.get(dni, estado);
	}

	@Override
	@Transactional
	public List<Reserva> get(String user, boolean estado) {
		// TODO Auto-generated method stub
		return dao.get(user, estado);
	}
}
