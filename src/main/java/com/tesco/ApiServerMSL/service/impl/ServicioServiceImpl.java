package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.ServicioDao;
import com.tesco.ApiServerMSL.model.Servicio;
import com.tesco.ApiServerMSL.service.ServicioService;

@Service
public class ServicioServiceImpl implements ServicioService{
	@Resource(name = "ServicioDao")
	private ServicioDao dao;

	@Override
	@Transactional
	public void save(Servicio servicio) {
		// TODO Auto-generated method stub
		dao.save(servicio);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Servicio servicio = this.get(id);
		if(servicio!=null){
			dao.delete(servicio);
		}
	}

	@Override
	@Transactional
	public Servicio get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Servicio> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}
}
