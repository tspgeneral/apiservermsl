package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.TargetaCreditoDao;
import com.tesco.ApiServerMSL.model.TargetaCredito;
import com.tesco.ApiServerMSL.service.TargetaCreditoService;
@Service
public class TargetaCreditoServiceImpl implements TargetaCreditoService{
	@Resource(name = "TargetaCreditoDao")
	private TargetaCreditoDao dao;

	@Override
	@Transactional
	public void save(TargetaCredito targetaCredito) {
		// TODO Auto-generated method stub
		dao.save(targetaCredito);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		TargetaCredito targetaCredito = this.get(id);
		if(targetaCredito!=null){
			dao.delete(targetaCredito);
		}
	}

	@Override
	@Transactional
	public TargetaCredito get(Long id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<TargetaCredito> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}

}
