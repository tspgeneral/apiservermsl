package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.UbicacionDao;
import com.tesco.ApiServerMSL.model.Ubicacion;
import com.tesco.ApiServerMSL.service.UbicacionService;
@Service
public class UbicacionServiceImpl implements UbicacionService{
	@Resource(name = "UbicacionDao")
	private UbicacionDao dao;
	
	@Override
	@Transactional
	public void save(Ubicacion ubicacion) {
		// TODO Auto-generated method stub
		dao.save(ubicacion);
	}

	@Override
	@Transactional
	public void delete(int id) {
		// TODO Auto-generated method stub
		Ubicacion ubicacion = this.get(id);
		if(ubicacion!=null){
			dao.delete(ubicacion);
		}
	}

	@Override
	@Transactional
	public Ubicacion get(int id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<Ubicacion> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}

}
