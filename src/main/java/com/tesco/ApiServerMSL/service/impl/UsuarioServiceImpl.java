package com.tesco.ApiServerMSL.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tesco.ApiServerMSL.dao.UsuarioDao;
import com.tesco.ApiServerMSL.model.UsuarioP;
import com.tesco.ApiServerMSL.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	@Resource(name = "UsuarioDao")
	private UsuarioDao dao;

	@Override
	@Transactional
	public void save(UsuarioP usuario) {
		// TODO Auto-generated method stub
		dao.save(usuario);
	}

	@Override
	@Transactional
	public void delete(String id) {
		// TODO Auto-generated method stub
		UsuarioP usuario = this.get(id);
		if (usuario != null) {
			dao.delete(usuario);
		}
	}

	@Override
	@Transactional
	public UsuarioP get(String id) {
		// TODO Auto-generated method stub
		return dao.get(id);
	}

	@Override
	@Transactional
	public List<UsuarioP> get() {
		// TODO Auto-generated method stub
		return dao.get();
	}
	
	@Override
	@Transactional
	public UsuarioP findByUserName(String username) {
		// TODO Auto-generated method stub
		return dao.findByUserName(username);
	}

}
