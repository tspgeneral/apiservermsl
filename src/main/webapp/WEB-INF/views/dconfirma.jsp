<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Añoranzas - Dashboard</title>

<link href="../resources/css/bootstrap.css" rel="stylesheet">
<!--<link href="css/datepicker3.css" rel="stylesheet">-->
<link href="../resources/css/styles.css" rel="stylesheet">
<script src="code.jquery.com/jquery-1.12.3.js"></script>
<!--Icons-->
<script src="../resources/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
	<c:url value="/web/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Añoranzas</span>
					Chaqueñas</a>
				<ul class="user-menu">
					<li class="dropdown pull-right"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"><svg
								class="glyph stroked male-user">
								<use xlink:href="#stroked-male-user"></use></svg>
							${pageContext.request.userPrincipal.name} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user">
										<use xlink:href="#stroked-male-user"></use></svg> Configuración</a></li>
							<!-- <li><a href="#"><svg class="glyph stroked gear"> -->
							<!-- <use xlink:href="#stroked-gear"></use></svg> Settings</a></li> -->
							<li><a href="javascript:formSubmit()"><svg
										class="glyph stroked cancel">
										<use xlink:href="#stroked-cancel"></use></svg> Salir</a></li>
						</ul></li>
				</ul>
			</div>

		</div>
		<!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="dreservas"><svg
						class="glyph stroked dashboard-dial">
						<use xlink:href="#stroked-calendar"></use></svg> Reservas</a></li>
			<li class="active"><a href="dconfirma"><svg
						class="glyph stroked calendar">
						<use xlink:href="#stroked-pencil"></use></svg> Confirmación</a></li>
			<li><a href="dcobro"><svg class="glyph stroked line-graph">
						<use xlink:href="#stroked-app-window-with-content"></use></svg> Pagos
					Factura</a></li>
		</ul>

	</div>
	<!--/.sidebar-->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Panel de confirmación</li>
			</ol>
		</div>
		<!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Confirmación</h1>
			</div>
		</div>
		<!--/.row-->
		<br>
	<!-- Cuerpo del dashboard -->

	<table class="table table-hover">
		<thead>
			<tr class="success">
				<td>idReserva</td>
				<td>Fecha Entrada</td>
				<td>Hora Entrada</td>
				<td>Confirmado</td>
				<td>Enviar</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listOfReservas}" var="artw">
				<tr>
					<td>${artw.idReserva}</td>
					<td>${artw.fechaIn}</td>
					<td>${artw.horaIn}</td>
					<td>${artw.estado}</td>
					<td><a
						href="confirmaRes/${artw.idReserva}/${pageContext.request.userPrincipal.name}">Confirmar</a></td>

				</tr>
			</c:forEach>
		</tbody>
	</table>

	<!----- end form ---->

	<!-- End Cuerpo del dashboard -->

	</div>
	<!--/.col-->
	</div>
	<!--/.row-->
	</div>
	<!--/.main-->

	<
	<script src="../resources/js/jquery.min.js"></script>
	<script src="../resources/js/bootstrap.js"></script>
	<!--<script src="./resources/js/chart.min.js"></script>
	<script src="./resources/js/chart-data.js"></script>
	<script src="./resources/js/easypiechart.js"></script>
	<script src="./resources/js/easypiechart-data.js"></script>
	<script src="./resources/js/bootstrap-datepicker.js"></script>-->
	<script>
		$('#calendar').datepicker({});

		!function($) {
			$(document)
					.on(
							"click",
							"ul.nav li.parent > a > span.icon",
							function() {
								$(this).find('em:first').toggleClass(
										"glyphicon-minus");
							});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function() {
			if ($(window).width() > 768)
				$('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function() {
			if ($(window).width() <= 767)
				$('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
