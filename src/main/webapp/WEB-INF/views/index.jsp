<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
	<head>
		
		<title>Website | Añoranzas Chaqueñas</title>
		<link href="./resources/css/bootstrap.css" rel='stylesheet' type='text/css' />
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="./resources/js/jquery.min.js"></script>
		 <!-- Custom Theme files -->
		 <!---- animated-css ---->
		<link href="./resources/css/animate.css" rel="stylesheet" type="text/css" media="all">
		<script src="./resources/js/wow.min.js"></script>
		<script>
		 new WOW().init();
		</script>
		<!---- animated-css ---->
		  <!---- start-smoth-scrolling---->
		<script type="text/javascript" src="./resources/js/move-top.js"></script>
		<script type="text/javascript" src="./resources/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		 <!---- start-smoth-scrolling---->
		<link href="./resources/css/style.css" rel='stylesheet' type='text/css' />
   		 <!-- Custom Theme files -->
   		 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!----webfonts--->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<!---webfonts --->
		<!-- <P>  The time on the server is ${serverTime}. </P> -->
	</head>
	<body>
		<div class="bg">
			<div class="container">
			<!----- start-header---->
			<div id="home" class="header wow bounceInDown" data-wow-delay="0.4s>
					<div class="top-header">
						<div class="logo wow bounceInDown" data-wow-delay="0.4s">
							<a href="/"><img src="./resources/images/logo4.png" title="dreams" /></a>
						</div>
						<!----start-top-nav---->
						 <nav class="top-nav wow bounceInDown" data-wow-delay="0.4s">
						  <span class="menu"> </span>
							<ul class="top-nav">
								<li class="active"><a href="/" class="scroll">Home</a></li>
								<li><a href="web/register">Registráte</a></li>
								<li><a href="web/dreservas">Ingresa</a></li>
								<li><a href="web/contact">Contacto</a></li>
							</ul>
						</nav>
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(200);
					});
				</script>

						<div class="clearfix"> </div>
					</div>
				</div>
				<!----- banner ---->
				<!----- banner ---->
				<div class="banner text-right">
					<div class="container">
						<h1>AÑORANZAS CHAQUEÑAS</h1>	
							<div class="clearfix"></div>
						<p>Disfruta un ambiente natural, aire libre, paseos, cabañas, pesca!</p>
						<div class="clearfix"> </div>
					</div>
				</div>
				<!----- banner ---->
			</div>
			<!----- //End-header---->
			</div>
		<!----- //End-bg4---->
			<!----- top-grids ----->
			<div class="container">
			<div class="welcome-note">
					<div class="welcome-note-grids">
						<div class="col-md-4 wow fadeInLeft" data-wow-delay="0.4s">
							<div class="welcome-note-left text-right">
								<h2>Bienvenido</h2>
								<span>Un lugar agradable</span>
							</div>
						</div> 
						<div class="col-md-8 welcome-grid">
							<div class="welcome-note-right ">
								<div class="col-md-8">
									<div class="welcome-note-right-left">
										<h3>Todo lo que necesitas para estar con la naturaleza.</h3>
										<p>Un lugar abierto con mucho campo, aire puro reconfortante, sentite cerca de la naturaleza.</p>
									</div>
								</div>
								<div class="col-md-4">
									<div class="welcome-note-right-right">
										<a href="#">Leer más</a>
									</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div> 
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
			<!----- top-grids ----->
			<!----- mid-grids ----->
			<div class="mid-grids">
				<div class="container">
					<div class="mid-grid">
							<div class="col-md-4 mid-grid-left  wow bounceInRight" data-wow-delay="0.4s">
								<img src="./resources/images/banner.jpg" title="name" />
							</div>
							<div class="col-md-8 mid-grid-right  wow bounceInLeft" data-wow-delay="0.4s">
								<h3>El complejo de ecoturístico Añoranzas Chaqueñas abrió sus puertas el día 17 junio de 2005</h3>
								<h4>El Restaurante y el camping surgieron primero y luego se desarrolló la posada y el museo  siempre con el objetivo de lograr el propósito de ofrecer el mejor servicio y calidad</h4>
								<p>Gracias al sueño de don Javier Leandro y su esposa Ana Cecilia, de abrir un camping y restaurante en “Villa Tranquila”, la cual era su quinta de descanso familiar, para albergar a los pescadores. Ese sueño bastó para que surgiera lo que hoy se conoce como Complejo EcoTurístico Añoranzas Chaqueñas.</p>
								<p>Para quien decida descansar y tomar un respiro sano, dejando volar sus sueños y añorando  el aroma del río, dentro de un ambiente rústico, acogedor, sano y totalmente familiar, en donde la clave del éxito ha sido sin duda alguna, su originalidad.</p>
							</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<!----- mid-grids ----->
			<!----- bottom-grids ----->
			<div class="bottom-grids">
				<div class="container">
					<div class="col-md-4 recent">
						<div class="recent-news">
							<h2>Ultimas noticias</h2>
							<div class="recent-news-grid wow fadeInLeft" data-wow-delay="0.5s">
								<div class="recent-news-left text-center">
									<span>10</span>
									<label>DIC</label>
								</div>
								<div class="recent-news-right">
									<h3>Torneo de pesca</h3>
									<span>Pesca Deportiva</span>
									<p>Para disfrutar de una tarde agradable con los mejores equipos y guías.</p>
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="recent-news-grid wow fadeInRight" data-wow-delay="0.5s">
								<div class="recent-news-left text-center">
									<span>24</span>
									<label>DIC</label>
								</div>
								<div class="recent-news-right">
									<h3>Fiestas</h3>
									<span>Festejo de navidad</span>
									<p>Podrás pasar la mejor navidad al aire libre. </p>
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="recent-news-grid wow fadeInLeft" data-wow-delay="0.5s">
								<div class="recent-news-left text-center">
									<span>31</span>
									<label>DIC</label>
								</div>
								<div class="recent-news-right">
									<h3>Año nuevo</h3>
									<span>Un momento especial</span>
									<p>Pasa la mejor despedida de año con una fiesta espectacular.  </p>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
					<!----->
				<div class="col-md-4 wow fadeInLeft" data-wow-delay="0.4s">
						<div class="about-us">
							<h2>Sobre nosotros</h2>
							<h4>Complejo Ecoturistico</h4>
							<img src="./resources/images/nosotros.jpg" title="name" />
							<p>Somos una posada-restaurante-museo-camping con vista al río ubicado  en Bajos del Tacuarí  a 30 km de Puerto Vilelas. El mejor lugar para respirar aire puro y sentirse en contacto con la naturaleza. </p>
						</div>
				</div>
				<!----->
				<!----->
				<div class="col-md-4">
						<div class="ourservices">
							<h2>Servicios</h2>
							<ul>
								<li><a href="#"><span> </span>Camping</a></li>
								<li><a href="#"><span> </span>Restaurante</a></li>
								<li><a href="#"><span> </span>Posada</a></li>
								<li><a href="#"><span> </span>Museo</a></li>
								<li><a href="#"><span> </span>Cabagaltas</a></li>
								<li><a href="#"><span> </span>Proveeduría</a></li>
								<li><a href="#"><span> </span>Paseo Náutico</a></li>
								<li><a href="#"><span> </span>Pesca</a></li>
							</ul>
						</div>
				</div>
				<!----->
				<div class="clearfix"> </div>
				</div>
			</div>
			<!----- bottom-grids ----->
			<!----- news-letter ----->
			<div class="news-letter wow fadeInLeft" data-wow-delay="0.4s">
				<div class="container">
					<div class="news-letter-grid">
						<div class="news-letter-grid-left">
							<h3>Hace tus reservas on-line!</h3>
							<p>Puedes registrar tus datos y hacer las reservas desde la pagina nos contactamos inmediatamente.</p>
						</div>
						<!--
						<div class="news-letter-grid-right">
							<form>
								<input type="text" class="text" maxlength="20" value="Your Email here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Your Email here';}">
								<input type="submit" value=" " />
							</form>
						</div>
						--->
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<!----- news-letter ----->
			<!---- footer ---->
			<div class="footer">
				<div class="container">
					<div class="footer-grids">
						<div class="col-md-3 footer-grid ftr-sec wow fadeInLeft" data-wow-delay="0.4s">
							<h3>Navegación</h3>
							<ul>
								<li><a href="/">home</a></li>
								<li><a href="web/register">Registráte</a></li>
								<li><a href="web/dreservas">ingresa</a></li>
								<li><a href="web/contact">contacto</a></li>							
							</ul>
						</div>
						<div class="col-md-3 footer-grid ftr-sec wow fadeInLeft" data-wow-delay="0.4s">
							<h3>Clientes</h3>
							<ul>
								<li><a href="#">Argentina</a></li>
								<li><a href="#">Brazil</a></li>
								<li><a href="#">Paraguay</a></li>
								<li><a href="#">Uruguay</a></li>								
							</ul>
						</div>
						<div class="col-md-3 footer-grid ftr-sec wow fadeInRight" data-wow-delay="0.4s">
							<h3>Síguenos</h3>
							<ul class="social-icons">
								<li><a class="twitter" href="#"><span> </span>twitter</a></li>
								<li><a class="facebook" href="#"><span> </span>Facebook</a></li>
								<li><a class="googlepluse" href="#"><span> </span>google+</a></li>								
							</ul>
						</div>
						<div class="col-md-3 footer-grid ftr-sec ftr wow fadeInRight" data-wow-delay="0.4s">
							<h3>Nuestra ubicación</h3>
							<ul class="location">
								<li><a class="hm" href="#"><span> </span>Bajos del Tacuarí  a 30 km de Puerto Vilelas</a></li>
								<li><a class="phn" href="#"><span> </span>Telefono: + 54 (0362) 044589.</a></li>
							</ul>							
						</div>
						<div class="clearfix"> </div>
					</div>
					<p class="copy-right wow bounceInRight" data-wow-delay="0.4s"">2016 &copy; Diseñado por <a href="http://tescoelectronics.net/">Portela Robledo</a></p>
				</div>
			</div>
			<!---- footer ---->
	</body>
</html>

