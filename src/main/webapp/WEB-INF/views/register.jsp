<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<title>Contacto | Añoranzas Chaqueñas</title>
<link href="../resources/css/bootstrap.css" rel='stylesheet'
	type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../resources/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!---- animated-css ---->
<link href="../resources/css/animate.css" rel="stylesheet"
	type="text/css" media="all">
<script src="../resources/js/wow.min.js"></script>
<script>
	new WOW().init();
</script>
<!---- animated-css ---->

<link href="../resources/css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript">
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 


</script>
<!----webfonts--->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<!---//webfonts--->
</head>
<body>
	<div class="bg4">
		<div class="container">
			<!----- start-header---->
			<div id="home" class="header">
				<div class="top-header">
					<div class="logo">
						<a href="/"><img src="../resources/images/logo4.png"
							title="dreams" /></a>
					</div>
					<!----start-top-nav---->
					<nav class="top-nav">
						<span class="menu"> </span>
						<ul class="top-nav">
							<li><a href="./">Home</a></li>
							<li class="active"><a href="register">Registráte</a></li>
							<li><a href="dreservas">Ingresa</a></li>
							<li><a href="contact">Contacto</a></li>
						</ul>
						<script>
							$("span.menu").click(function() {
								$(".top-nav ul").slideToggle(200);
							});
						</script>
					</nav>
					<div class="clearfix"></div>
				</div>
			</div>
			<!----- banner ---->
			<div class="banner text-right">
				<div class="container">
					<h1>Registráte</h1>
					<div class="clearfix"></div>
					<p>Podrás hacer reservas de los servicios y confirmarlos.</p>
					<div class="clearfix"></div>
				</div>
			</div>
			<!----- banner ---->
		</div>
		<!----- //End-header---->
	</div>
	<!----- //End-bg4---->
	<div class="container">
		<br> <br>
		<h3 class="style">Registrar :</h3>
		<br> <br>
		<c:if test="${not empty msg}">
			<div class="alert alert-${css} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<strong>${msg}</strong>
			</div>
		</c:if>

		<!----- form ---->
		<form:form class="form-horizontal" role="form"
			action="register?${_csrf.parameterName}=${_csrf.token}">
			<!----- UsuarioP ---->
			<!-- 	 spring:bin used show error  -->
			<spring:bind path="usuarioP.usuario">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="usuario" class="control-label col-md-2">Usuario
						:</label>
					<div class="col-md-5">
						<form:input path="usuarioP.usuario" type="text"
							class="form-control" id="inputSuccess" placeholder="usuario" />
						<form:errors path="usuarioP.usuario" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="usuarioP.password">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="password" class="control-label col-md-2">Password
						:</label>
					<div class="col-md-5">
						<form:input path="usuarioP.password" type="password"
							class="form-control" id="inputSuccess" placeholder="password" />
						<form:errors path="usuarioP.password" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="usuarioP.email">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="email" class="control-label col-md-2">E-Mail :</label>
					<div class="col-md-5">
						<form:input path="usuarioP.email" type="email"
							class="form-control" id="mail" placeholder="email" />
						<form:errors path="usuarioP.email" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<!-----End UsuarioP ---->

			<!-- Ubicacion -->

			<spring:bind path="ubicacion.pais">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="pais" class="control-label col-md-2">Pais :</label>
					<div class="col-md-5">
						<form:input path="ubicacion.pais" type="text" class="form-control"
							id="ubicacion.pais" placeholder="pais" />
						<form:errors path="ubicacion.pais" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="ubicacion.provincia">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="provincia" class="control-label col-md-2">Provincia
						:</label>
					<div class="col-md-5">
						<form:input path="ubicacion.provincia" type="text"
							class="form-control" id="provincia" placeholder="provincia" />
						<form:errors path="ubicacion.provincia" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="ubicacion.ciudad">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="ciudad" class="control-label col-md-2">Ciudad :</label>
					<div class="col-md-5">
						<form:input path="ubicacion.ciudad" type="text"
							class="form-control" id="ciudad" placeholder="ciudad" />
						<form:errors path="ubicacion.ciudad" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="ubicacion.direccion">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="direccion" class="control-label col-md-2">Dirección
						:</label>
					<div class="col-md-5">
						<form:input path="ubicacion.direccion" type="text"
							class="form-control" id="direccion" placeholder="direccion" />
						<form:errors path="ubicacion.direccion" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<!-- End Ubicacion -->

			<!-- Persona -->
			<spring:bind path="persona.dni">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="dni" class="control-label col-md-2">DNI :</label>
					<div class="col-md-5">
						<form:input path="persona.dni" type="number" class="form-control"
							id="person.dni" placeholder="dni" />
						<form:errors path="persona.dni" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="persona.nombre">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="nombre" class="control-label col-md-2">Nombre :</label>
					<div class="col-md-5">
						<form:input path="persona.nombre" type="text" class="form-control"
							id="persona.nombre" placeholder="nombre" />
						<form:errors path="persona.nombre" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="persona.apellido">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="apellido" class="control-label col-md-2">Apellido
						:</label>
					<div class="col-md-5">
						<form:input path="persona.apellido" type="text"
							class="form-control" id="persona.apellido" placeholder="apellido" />
						<form:errors path="persona.apellido" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="persona.telefono">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="telefono" class="control-label col-md-2">Telefono
						:</label>
					<div class="col-md-5">
						<form:input path="persona.telefono" type="number"
							class="form-control" id="telefono" placeholder="telefono" />
						<form:errors path="persona.telefono" class="control-label" />
					</div>
				</div>
			</spring:bind>


			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-8">
					<button type="submit" class="btn btn-primary">Enviar</button>
				</div>
			</div>


			<!-- End Persona -->

		</form:form>

		<!----- end form ---->

	</div>
	<!---- footer ---->
	<div class="footer">
		<div class="container">
			<div class="footer-grids">
				<div class="col-md-3 footer-grid ftr-sec wow fadeInLeft"
					data-wow-delay="0.4s">
					<h3>Navegación</h3>
					<ul>
						<li><a href="./">home</a></li>
						<li><a href="register">Registráte</a></li>
						<li><a href="dreservas">ingresa</a></li>
						<li><a href="contact">contacto</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid ftr-sec wow fadeInLeft"
					data-wow-delay="0.4s">
					<h3>Clientes</h3>
					<ul>
						<li><a href="#">Argentina</a></li>
						<li><a href="#">Brazil</a></li>
						<li><a href="#">Paraguay</a></li>
						<li><a href="#">Uruguay</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid ftr-sec wow fadeInRight"
					data-wow-delay="0.4s">
					<h3>Síguenos</h3>
					<ul class="social-icons">
						<li><a class="twitter" href="#"><span> </span>twitter</a></li>
						<li><a class="facebook" href="#"><span> </span>Facebook</a></li>
						<li><a class="googlepluse" href="#"><span> </span>google+</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid ftr-sec ftr wow fadeInRight"
					data-wow-delay="0.4s">
					<h3>Nuestra ubicación</h3>
					<ul class="location">
						<li><a class="hm" href="#"><span> </span>Bajos del
								Tacuarí a 30 km de Puerto Vilelas</a></li>
						<li><a class="phn" href="#"><span> </span>Telefono: + 54
								(0362) 044589.</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
			<p class="copy-right wow bounceInRight" data-wow-delay="0.4s"">
				2016 &copy; Diseñado por <a href="http://tescoelectronics.net/">Portela
					Robledo</a>
			</p>
		</div>
	</div>
	<!---- footer ---->
</body>
</html>