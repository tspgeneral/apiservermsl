<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%-- <%@ page session="false"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>Contacto | Añoranzas Chaqueñas</title>
<link href="../resources/css/bootstrap.css" rel='stylesheet'
	type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../resources/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!---- animated-css ---->
<link href="../resources/css/animate.css" rel="stylesheet"
	type="text/css" media="all">
<script src="../resources/js/wow.min.js"></script>
<script>
	new WOW().init();
</script>
<!---- animated-css ---->

<link href="../resources/css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript">
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 



</script>
<!----webfonts--->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<!---//webfonts--->
</head>
<body>
	<div class="bg4">
		<div class="container">
			<!----- start-header---->
			<div id="home" class="header">
				<div class="top-header">
					<div class="logo">
						<a href="/"><img src="../resources/images/logo4.png"
							title="dreams" /></a>
					</div>
					<!----start-top-nav---->
					<nav class="top-nav">
						<span class="menu"> </span>
						<ul class="top-nav">
							<li><a href="./">Home</a></li>
							<li><a href="register">Registráte</a></li>
							<li><a href="dreservas">Ingresa</a></li>
							<li class="active"><a href="contact">Contacto</a></li>
						</ul>
						<script>
							$("span.menu").click(function() {
								$(".top-nav ul").slideToggle(200);
							});
						</script>
					</nav>
					<div class="clearfix"></div>
				</div>
			</div>
			<!----- banner ---->
			<div class="banner text-right">
				<div class="container">
					<h1>Contacto</h1>
					<div class="clearfix"></div>
					<p>Si esta interesado y necesitas comunicarte, envianos tu
						inquietud.</p>
					<div class="clearfix"></div>
				</div>
			</div>
			<!----- banner ---->
		</div>
		<!----- //End-header---->
	</div>
	<!----- //End-bg4---->
	<div class="container">
		<div class="contact-us">
			<div class="contact-us_left">
				<div class="contact-us_info">
					<h3 class="style">Encuéntranos</h3>
					<div class="map">
						<iframe
							src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28307.788447429964!2d-58.96447942755491!3d-27.516743303746814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x944572886bd59a7d%3A0x9bdce8bf00c7391a!2sPuerto+Vilelas%2C+Chaco+Province%2C+Argentina!5e0!3m2!1sen!2sus!4v1476146636346"
							width="400" height="300" frameborder="0" style="border: 0"
							allowfullscreen></iframe>
					</div>
				</div>
				<div class="company_address">
					<h3 class="style">Información del complejo :</h3>
					<p>Bajos del Tacuarí a 30 km de Puerto Vilelas,</p>
					<p>Provincia del Chaco,</p>
					<p>Argentina</p>
					<p>Telefono:+ 54 (0362) 044589</p>
					<p>Fax: + 54 (0362) 044589</p>
					<p>
						Email: <a href="mailto:ecoturismo@gmail.com">ecoturismo(at)gmail.com</a>
					</p>
					<p>
						Síguenos en: <a href="#">Facebook</a>, <a href="#">Twitter</a>
					</p>
				</div>
			</div>
			<div class="contact_right">
				<div class="contact-form">
					<h3 class="style">Contáctenos</h3>
					<form method="post" action="#">
						<div>
							<p>NOMBRE</p>
							<span><input name="userName" type="text" class="textbox"></span>
						</div>
						<div>
							<p>E-MAIL</p>
							<span><input name="userEmail" type="text" class="textbox"></span>
						</div>
						<div>
							<p>CELULAR</p>
							<span><input name="userPhone" type="text" class="textbox"></span>
						</div>
						<div>
							<p>MENSAJE</p>
							<span><textarea name="userMsg"> </textarea></span>
						</div>
						<div>
							<input type="submit" value="enviar">
						</div>
					</form>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!---- footer ---->
	<div class="footer">
		<div class="container">
			<div class="footer-grids">
				<div class="col-md-3 footer-grid ftr-sec wow fadeInLeft"
					data-wow-delay="0.4s">
					<h3>Navegación</h3>
					<ul>
						<li><a href="/">home</a></li>
						<li><a href="register">Registráte</a></li>
						<li><a href="dreservas">ingresa</a></li>
						<li><a href="contact">contacto</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid ftr-sec wow fadeInLeft"
					data-wow-delay="0.4s">
					<h3>Clientes</h3>
					<ul>
						<li><a href="#">Argentina</a></li>
						<li><a href="#">Brazil</a></li>
						<li><a href="#">Paraguay</a></li>
						<li><a href="#">Uruguay</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid ftr-sec wow fadeInRight"
					data-wow-delay="0.4s">
					<h3>Síguenos</h3>
					<ul class="social-icons">
						<li><a class="twitter" href="#"><span> </span>twitter</a></li>
						<li><a class="facebook" href="#"><span> </span>Facebook</a></li>
						<li><a class="googlepluse" href="#"><span> </span>google+</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-grid ftr-sec ftr wow fadeInRight"
					data-wow-delay="0.4s">
					<h3>Nuestra ubicación</h3>
					<ul class="location">
						<li><a class="hm" href="#"><span> </span>Bajos del
								Tacuarí a 30 km de Puerto Vilelas</a></li>
						<li><a class="phn" href="#"><span> </span>Telefono: + 54
								(0362) 044589.</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
			<p class="copy-right wow bounceInRight" data-wow-delay="0.4s"">
				2016 &copy; Diseñado por <a href="http://tescoelectronics.net/">Portela
					Robledo</a>
			</p>
		</div>
	</div>
	<!---- footer ---->
</body>
</html>