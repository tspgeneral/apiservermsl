<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Añoranzas - Dashboard</title>

<link href="../resources/css/bootstrap.css" rel="stylesheet">
<!--<link href="css/datepicker3.css" rel="stylesheet">-->
<link href="../resources/css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="../resources/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="../resources/css/jquery.dataTables.min.css"
	type="text/css">
<script src="../resources/js/jquery-1.12.3.js"></script>
<script src="../resources/js/jquery.dataTables.min.js"></script>

</head>

<body>
	<c:url value="/web/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Añoranzas</span>
					Chaqueñas</a>
				<ul class="user-menu">
					<li class="dropdown pull-right"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"><svg
								class="glyph stroked male-user">
								<use xlink:href="#stroked-male-user"></use></svg>
							${pageContext.request.userPrincipal.name} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user">
										<use xlink:href="#stroked-male-user"></use></svg> Configuración</a></li>
							<!-- <li><a href="#"><svg class="glyph stroked gear"> -->
							<!-- <use xlink:href="#stroked-gear"></use></svg> Settings</a></li> -->
							<li><a href="javascript:formSubmit()"><svg
										class="glyph stroked cancel">
										<use xlink:href="#stroked-cancel"></use></svg> Salir</a></li>
						</ul></li>
				</ul>
			</div>

		</div>
		<!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li><a href="dreservas"><svg
						class="glyph stroked dashboard-dial">
						<use xlink:href="#stroked-calendar"></use></svg> Reservas</a></li>
			<li><a href="dconfirma"><svg class="glyph stroked calendar">
						<use xlink:href="#stroked-pencil"></use></svg> Confirmación</a></li>
			<li class="active"><a href="dcobro"><svg
						class="glyph stroked line-graph">
						<use xlink:href="#stroked-app-window-with-content"></use></svg> Pagos
					Factura</a></li>
		</ul>

	</div>
	<!--/.sidebar-->

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Panel de pagos</li>
			</ol>
		</div>
		<!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Reservas</h1>
			</div>
		</div>
		<!--/.row-->
		<br> <br>
		<c:if test="${not empty msg}">
			<div class="alert alert-${css} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<strong>${msg}</strong>
			</div>
		</c:if>
		<!-- Cuerpo del dashboard -->
		<table id="example" class="table table-hover display">
			<thead>
				<tr>
					<th>ID</th>
					<th>Fecha Ent.</th>
					<th>Fecha Sal.</th>
					<th>Hora Ent.</th>
					<th>Hora Sal.</th>
					<th>Servicio</th>
					<th>Costo ($)</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>Fecha Ent.</th>
					<th>Fecha Sal.</th>
					<th>Hora Ent.</th>
					<th>Hora Sal.</th>
					<th>Servicio</th>
					<th>Costo ($)</th>
				</tr>
			</tfoot>
			<tbody>
				<c:forEach var="reservas" items="${listOfReservas}">
					<tr id="row_${reservas.idReserva}">
						<td>${reservas.idReserva}</td>
						<td>${reservas.fechaIn}</td>
						<td>${reservas.fechaOut}</td>
						<td>${reservas.horaIn}</td>
						<td>${reservas.horaOut}</td>
						<td>${reservas.idServicio.nombre}</td>
						<td>${reservas.idServicio.costo}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<!-- 		formulario -->
		<form:form class="form-horizontal" role="form"
			action="dcobro?${_csrf.parameterName}=${_csrf.token}">

			<spring:bind path="montoTotal">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="montoTotal" class="control-label col-md-2">Monto
						Total</label>
					<div class="col-md-5">
						<form:input path="montoTotal" type="text" class="form-control"
							id="montoTotal" placeholder="montoTotal" value="${costoTotal}" />
						<form:errors path="montoTotal" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="fecha">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="fecha" class="control-label col-md-2">Fecha</label>
					<div class="col-md-5">
						<form:input path="fecha" class="form-control" type="date"
							placeholder="Fecha" id="theDate" />
						<form:errors path="fecha" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="tipo">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="tipo" class="control-label col-md-2">Tipo Fac.</label>
					<div class="col-md-5">
						<form:select path="tipo" id="tipo" class="form-control"
							placeholder="tipo">
							<form:option value="Factura A">
								Factura A
							</form:option>
							<form:option value="Factura B">
								Factura B
							</form:option>
							<form:option value="Factura C">
								Factura C
							</form:option>
						</form:select>
					</div>
				</div>
			</spring:bind>

			<spring:bind path="numTargeta">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="numTargeta" class="control-label col-md-2">Num. Targeta</label>
					<div class="col-md-5">
						<form:input path="numTargeta" type="number"
							class="form-control" id="numTargeta"
							placeholder="Num. Targeta" />
						<form:errors path="numTargeta" class="control-label" />
					</div>
				</div>
			</spring:bind>
			
			<spring:bind path="fechaVencimiento">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="fechaVencimiento" class="control-label col-md-2">Fecha Ven.</label>
					<div class="col-md-5">
						<form:input path="fechaVencimiento" class="form-control" type="date"
							placeholder="Fecha Ven." />
						<form:errors path="fechaVencimiento" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="codSeguridad">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="codSeguridad" class="control-label col-md-2">Cod. Seguridad</label>
					<div class="col-md-5">
						<form:input path="codSeguridad" type="number"
							class="form-control" id="numTargeta"
							placeholder="Cod. Seguridad" />
						<form:errors path="codSeguridad" class="control-label" />
					</div>
				</div>
			</spring:bind>
			
			<spring:bind path="cuotas">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="cuotas" class="control-label col-md-2">Cuotas</label>
					<div class="col-md-5">
						<form:input path="cuotas" type="number"
							class="form-control" id="cuotas"
							placeholder="Cuotas" />
						<form:errors path="cuotas" class="control-label" />
					</div>
				</div>
			</spring:bind>
			

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-8">
					<button type="submit" class="btn btn-primary">Enviar</button>
				</div>
			</div>
		</form:form>

		<!----- end form ---->


		<!-- End Cuerpo del dashboard -->

	</div>
	<!--/.col-->
	</div>
	<!--/.row-->
	</div>
	<!--/.main-->

	<!--<script<script src="../resources/js/jquery.min.js"></script>-->
	<script src="../resources/js/bootstrap.js"></script>
	<!--<script src="./resources/js/chart.min.js"></script>
	<script src="./resources/js/chart-data.js"></script>
	<script src="./resources/js/easypiechart.js"></script>
	<script src="./resources/js/easypiechart-data.js"></script>
	<script src="./resources/js/bootstrap-datepicker.js"></script>-->
	<script>
		$(document).ready(function() {
			$('#example').DataTable();
			
			 var date = new Date();

			    var day = date.getDate();
			    var month = date.getMonth() + 1;
			    var year = date.getFullYear();

			    if (month < 10) month = "0" + month;
			    if (day < 10) day = "0" + day;

			    var today = year + "-" + month + "-" + day;       
			    $("#theDate").attr("value", today);
		});
		$('#calendar').datepicker({});

		!function($) {
			$(document)
					.on(
							"click",
							"ul.nav li.parent > a > span.icon",
							function() {
								$(this).find('em:first').toggleClass(
										"glyphicon-minus");
							});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function() {
			if ($(window).width() > 768)
				$('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function() {
			if ($(window).width() <= 767)
				$('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
