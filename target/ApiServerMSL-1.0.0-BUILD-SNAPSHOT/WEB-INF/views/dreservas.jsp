<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Añoranzas - Dashboard</title>

<link href="../resources/css/bootstrap.css" rel="stylesheet">
<!--<link href="css/datepicker3.css" rel="stylesheet">-->
<link href="../resources/css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="../resources/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet"
	href="../resources/css/jquery.dataTables.min.css"
	type="text/css">
<script src="../resources/js/jquery-1.12.3.js"></script>
<script src="../resources/js/jquery.dataTables.min.js"></script>

</head>

<body>
	<c:url value="/web/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Añoranzas</span>
					Chaqueñas</a>
				<ul class="user-menu">
					<li class="dropdown pull-right"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"><svg
								class="glyph stroked male-user">
								<use xlink:href="#stroked-male-user"></use></svg>
							${pageContext.request.userPrincipal.name} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#"><svg class="glyph stroked male-user">
										<use xlink:href="#stroked-male-user"></use></svg> Configuración</a></li>
							<!-- <li><a href="#"><svg class="glyph stroked gear"> -->
							<!-- <use xlink:href="#stroked-gear"></use></svg> Settings</a></li> -->
							<li><a href="javascript:formSubmit()"><svg
										class="glyph stroked cancel">
										<use xlink:href="#stroked-cancel"></use></svg> Salir</a></li>
						</ul></li>
				</ul>
			</div>

		</div>
		<!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="active"><a href="dreservas"><svg
						class="glyph stroked dashboard-dial">
						<use xlink:href="#stroked-calendar"></use></svg> Reservas</a></li>
			<li><a href="dconfirma"><svg class="glyph stroked calendar">
						<use xlink:href="#stroked-pencil"></use></svg> Confirmación</a></li>
			<li><a href="dcobro"><svg class="glyph stroked line-graph">
						<use xlink:href="#stroked-app-window-with-content"></use></svg> Cobro
					Pagos</a></li>
		</ul>

	</div>
	<!--/.sidebar-->

	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Panel de reservas y confirmación</li>
			</ol>
		</div>
		<!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Reservas</h1>
			</div>
		</div>
		<!--/.row-->
		<br>
		<br>
		<c:if test="${not empty msg}">
			<div class="alert alert-${css} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<strong>${msg}</strong>
			</div>
		</c:if>
		<!-- Cuerpo del dashboard -->
		<form:form class="form-horizontal" role="form"
			action="dreservas?${_csrf.parameterName}=${_csrf.token}">

			<spring:bind path="usuario">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="usuario" class="control-label col-md-2">Usuario</label>
					<div class="col-md-5">
						<form:input path="usuario" type="text" class="form-control"
							id="usuario" placeholder="Usuario"
							value="${pageContext.request.userPrincipal.name}" />
						<form:errors path="usuario" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="fechaIn">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="fechaIn" class="control-label col-md-2">Fecha
						Entrada</label>
					<div class="col-md-5">
						<form:input path="fechaIn" class="form-control" type="date"
							placeholder="Fecha Entrada" />
						<form:errors path="fechaIn" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="horaIn">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="horaIn" class="control-label col-md-2">Hora
						Entrada</label>
					<div class="col-md-5">
						<form:input path="horaIn" class="form-control" type="time"
							placeholder="Hora Entrada" />
						<form:errors path="horaIn" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="fechaOut">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="fechaOut" class="control-label col-md-2">Fecha
						Salida</label>
					<div class="col-md-5">
						<form:input path="fechaOut" class="form-control" type="date"
							placeholder="Fecha Salida" />
						<form:errors path="fechaOut" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="horaOut">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="horaOut" class="control-label col-md-2">Hora
						Salida</label>
					<div class="col-md-5">
						<form:input path="horaOut" class="form-control" type="time"
							placeholder="Hora Salida" />
						<form:errors path="horaOut" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="reserva.cantPersona">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="reserva.cantPersona" class="control-label col-md-2">Can.
						Personas</label>
					<div class="col-md-5">
						<form:input path="reserva.cantPersona" type="number"
							class="form-control" id="reserva.cantPersona"
							placeholder="Cant. Persona" />
						<form:errors path="reserva.cantPersona" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="reserva.estado">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label for="reserva.estado" class="control-label col-md-2">Confirma</label>
					<div class="col-md-5">
						<form:select path="reserva.estado" id="reserva.estado">
							<form:option value="false">falso</form:option>
							<form:option value="true">verdadero</form:option>
						</form:select>
						<form:errors path="reserva.estado" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="idServicio">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">Servicios</label>
					<div class="col-sm-10">
						<form:radiobuttons path="idServicio" items="${serviciosMap}"
							element="label class='radio'" />
						<br />
						<form:errors path="idServicio" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-8">
					<button type="submit" class="btn btn-primary">Enviar</button>
				</div>
			</div>
		</form:form>

		<!----- end form ---->
		<br> <br>
		<table id="example" class="table table-hover display">
			<thead>
				<tr>
					<th>ID</th>
					<th>Fecha Ent.</th>
					<th>Fecha Sal.</th>
					<th>Hora Ent.</th>
					<th>Hora Sal.</th>
					<th>Cant. Personas</th>
					<th>Estado</th>
					<th>Servicio</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>ID</th>
					<th>Fecha Ent.</th>
					<th>Fecha Sal.</th>
					<th>Hora Ent.</th>
					<th>Hora Sal.</th>
					<th>Cant. Personas</th>
					<th>Estado</th>
					<th>Servicio</th>
				</tr>
			</tfoot>
			<tbody>
				<c:forEach var="reservas" items="${listOfReservas}">
					<tr id="row_${reservas.idReserva}">
						<td>${reservas.idReserva}</td>
						<td>${reservas.fechaIn}</td>
						<td>${reservas.fechaOut}</td>
						<td>${reservas.horaIn}</td>
						<td>${reservas.horaOut}</td>
						<td>${reservas.cantPersona}</td>
						<td>${reservas.estado}</td>
						<td><a href="Show/${reservas.idReserva}">${reservas.idServicio.nombre}</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<!-- End Cuerpo del dashboard -->

	</div>
	<!--/.col-->
	</div>
	<!--/.row-->
	</div>
	<!--/.main-->

	<!--<script<script src="../resources/js/jquery.min.js"></script>-->
	<script src="../resources/js/bootstrap.js"></script>
	<!--<script src="../resources/js/chart.min.js"></script>
	<script src="../resources/js/chart-data.js"></script>
	<script src="../resources/js/easypiechart.js"></script>
	<script src="../resources/js/easypiechart-data.js"></script>
	<script src="../resources/js/bootstrap-datepicker.js"></script>-->
	<script>
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
		$('#calendar').datepicker({});

		!function($) {
			$(document)
					.on(
							"click",
							"ul.nav li.parent > a > span.icon",
							function() {
								$(this).find('em:first').toggleClass(
										"glyphicon-minus");
							});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function() {
			if ($(window).width() > 768)
				$('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function() {
			if ($(window).width() <= 767)
				$('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
